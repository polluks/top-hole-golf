// Copyright 2020 Matthew Clarke

// Program will look at all 'backdrop' files in the tiles directory and
// re-write the address (in the first two bytes of the file) so that data
// is written directly into the 'bdrops2' module.

constant TILES_DIR = "/home/matthew/programming/6510assembly/golfgit/commodore-golf/assets/tiles";
mapping HEX_MAP = ([ "0":0, "1":1, "2":2, "3":3, "4":4, "5":5, "6":6, "7":7,
        "8":8, "9":9, "a":10, "b":11, "c":12, "d":13, "e":14, "f":15 ]);

int main(int argc, array(string) argv)
{
    // Single command-line argument is a line from grep output.
    string line = argv[1];

    // We must find the address.
    int i = search(line, "$");
    string addr = line[ i+1 .. i+4 ];
    int hex_addr = str2hex(addr);

    // Pick out all the backdrop files from the tiles directory.
    // We are going to edit just the first two bytes.
    object regex = Regexp.PCRE._pcre("b\\d\\d\\.bin");
    array(string) all_files = get_dir(TILES_DIR);
    foreach (all_files, string f)
    {
        if ( arrayp(regex->exec(f)) )
        {
            process_file(f, hex_addr);
        } // if
    } // foreach
    return 0;

} // main()

int str2hex(string s)
{
    int val = 0;
    int exponent = 0;

    for (int i = (sizeof(s)-1); i >= 0; --i)
    {
        string digit = s[ i .. i ];
        val += HEX_MAP[digit] * pow(2, exponent);
        exponent += 4;
    } // for
    return val;

} // str2hex()

void process_file(string file, int addr)
{
    string path = sprintf("%s/%s", TILES_DIR, file);
    string data = Image.load_file(path);
    data[0] = addr & 0xff;
    data[1] = (addr >> 8) & 0xff;
    object fout = Stdio.File(path, "wct");
    fout->write("%s", data);
    fout->close();

} // process_file

