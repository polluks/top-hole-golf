; Top-hole Golf
; Copyright 2020 Matthew Clarke


; BUG: this module is broken - can play only one channel.

; *****************
; *** CONSTANTS ***
; *****************
snd_c_NUM_CHANNELS = 3

snd_l_REGS_BASE_ADDR_LO   !byte   $00,$07,$0e
snd_l_REGS_BASE_ADDR_HI   !byte   $d4,$d4,$d4

; Filter types.
snd_c_FILTER_LOWPASS  = 2<<4
snd_c_FILTER_BANDPASS = 2<<5
snd_c_FILTER_HIGHPASS = 2<<6


; *****************
; *** VARIABLES ***
; *****************
snd_v_channel_active    !fill   snd_c_NUM_CHANNELS,0
snd_v_counter           !fill   snd_c_NUM_CHANNELS,0
snd_v_data_offset       !fill   snd_c_NUM_CHANNELS,0
snd_v_must_loop         !fill   snd_c_NUM_CHANNELS,0

snd_v_data_address_lo   !fill   snd_c_NUM_CHANNELS,0    
snd_v_data_address_hi   !fill   snd_c_NUM_CHANNELS,0     


; *******************
; ****** MACROS *****
; *******************


; *******************
; *** SUBROUTINES ***
; *******************
!zone {
snd_s_clear_regs
    lda #0
    ldx #SIGVOL-FRELO1
-   sta FRELO1,x
    dex
    bpl -
    rts
; end sub snd_s_clear_regs
} ; !zone

; **************************************************

; INPUTS:   Y = sfx #, [A = volume]
; OUTPUTS:  X = channel/voice
!zone {
.VOLUME = ROT0 

snd_s_init_sfx
    lda #$0f
snd_s_init_sfx_with_volume
    sta .VOLUME

    jsr snd_s_next_free_channel
    bcs .end

    lda .VOLUME
    sta SIGVOL

    lda sfx_l_INIT_ADDR_LO,y
    sta SND_INIT_DATA_ZP_LO
    lda sfx_l_INIT_ADDR_HI,y
    sta SND_INIT_DATA_ZP_HI
    lda sfx_l_DATA_ADDR_LO,y
    sta snd_v_data_address_lo,x
    lda sfx_l_DATA_ADDR_HI,y
    sta snd_v_data_address_hi,x
    
    jsr snd_s_real_init

.end
    rts
; end sub snd_s_init_sfx
} ; !zone

; **************************************************

; OUTPUT: X holds channel number.  
; If C flag set, no channel was found.
!zone {
snd_s_next_free_channel
    clc
    ldx #snd_c_NUM_CHANNELS-1
-   lda snd_v_channel_active,x
    beq .found
    dex
    bpl -
    ; No free channel.
    sec
.found
    rts
; end sub snd_s_next_free_channel
} ; !zone

; **************************************************

; INPUT: X = channel (0 to 2).
!zone {
snd_s_real_init
    ; First initialize SND_REGS_BASE zero page vector.
    lda snd_l_REGS_BASE_ADDR_LO,x
    sta SND_REGS_BASE_ZP_LO 
    lda snd_l_REGS_BASE_ADDR_HI,x
    sta SND_REGS_BASE_ZP_HI 

    ; Activate this channel and reset data offset.
    inc snd_v_channel_active,x
    lda #0
    sta snd_v_data_offset,x

    ; Is a filter to be used?
    ldy #9
    lda (SND_INIT_DATA_ZP_LO),y
    ; TODO: must turn off any existing filter?
    beq .skip_filter
    pha ; Push filter mode onto stack for later.
    ; Record that current voice will have a filter.
    lda RESON   
    ora utils_l_BIT_LOOKUP,x
    sta RESON
    ; Filter mode - currently on top of stack.
    pla
    ora SIGVOL
    sta SIGVOL
    ; Resonance.
    ldy #10
    lda (SND_INIT_DATA_ZP_LO),y
    ora RESON
    sta RESON
    ; Cutoff - lo/hi bytes.
    ldy #11
    lda (SND_INIT_DATA_ZP_LO),y
    sta CUTLO
    ldy #12
    lda (SND_INIT_DATA_ZP_LO),y
    sta CUTHI
    jmp .filter_in_use

.skip_filter
    ; Turn off filter for this voice.
    lda utils_l_EOR_BIT_LOOKUP,x
    and RESON
    sta RESON

.filter_in_use
    ; Set the initial sound data.
    ldy #6
-   lda (SND_INIT_DATA_ZP_LO),y
    sta (SND_REGS_BASE_ZP_LO),y
    dey
    bpl -

    ; VCREG last.  Offset from beginning of init data is 7.  Offset from
    ; FRELOx is 4.
    ldy #7
    lda (SND_INIT_DATA_ZP_LO),y
    ldy #4
    sta (SND_REGS_BASE_ZP_LO),y
    
    ldy #8
    lda (SND_INIT_DATA_ZP_LO),y
    sta snd_v_counter,x

    ; Must loop?
    ldy #13
    lda (SND_INIT_DATA_ZP_LO),y
    sta snd_v_must_loop,x

.end
    rts
; end sub snd_real_init
} ; !zone

; **************************************************

!zone {
snd_s_update
    ldx #snd_c_NUM_CHANNELS-1 
-   lda snd_v_channel_active,x
    beq .next

    dec snd_v_counter,x
    bne .next

    ; Need to look at the next chunk of frequency data.  
    ; Load relevant source address into zp memory.
    lda snd_v_data_address_lo,x     
    sta SND_INIT_DATA_ZP_LO 
    lda snd_v_data_address_hi,x     
    sta SND_INIT_DATA_ZP_HI 
    ; And the destination address.
    lda snd_l_REGS_BASE_ADDR_LO,x   
    sta SND_REGS_BASE_ZP_LO 
    lda snd_l_REGS_BASE_ADDR_HI,x   
    sta SND_REGS_BASE_ZP_HI 

.read_data
    ldy snd_v_data_offset,x         

    ; Get the next byte of data.  Check if it's $ff (- that's the end of 
    ; the sound effect).
    lda (SND_INIT_DATA_ZP_LO),y
    cmp #$ff
    bne +
    ; End of sound effect has been reached.
    ; Must loop?
    lda snd_v_must_loop,x
    beq .deactivate
    lda #0
    sta snd_v_data_offset,x
    jmp .read_data

.deactivate
    ; Deactivate channel.
    lda #0
    sta snd_v_channel_active,x
    ; Close gate bit for channel.
    iny
    lda (SND_INIT_DATA_ZP_LO),y
    ldy #VCREG1-FRELO1
    sta (SND_REGS_BASE_ZP_LO),y
    rts

+   ; Must be new value for frequency (low byte).
    ; Push FRELO and FREHI onto stack.  We'll deal with them later because
    ; we'll need the Y register to write those values to the SID regs.
    pha
    iny
    lda (SND_INIT_DATA_ZP_LO),y
    pha
    iny
    ; Counter we can store straight away though.
    lda (SND_INIT_DATA_ZP_LO),y
    sta snd_v_counter,x

    ; Increment again and save as offset for next time.
    iny
    tya
    sta snd_v_data_offset,x

    ; Pull FREHI and FRELO off stack and write to relevant SID registers.
    ldy #1
    pla
    sta (SND_REGS_BASE_ZP_LO),y
    dey
    pla
    sta (SND_REGS_BASE_ZP_LO),y

.next
    dex
    bpl -
    rts
; end sub snd_s_update
} ; !zone

; **************************************************

; INPUTS:   X = voice
!zone {
snd_s_kill_voice
    lda #0
    sta snd_v_channel_active,x
    lda snd_l_REGS_BASE_ADDR_LO,x
    sta P0
    lda snd_l_REGS_BASE_ADDR_HI,x
    sta P1
    ldy #4
    lda #0
    sta (P0),y
    rts
; end sub snd_s_kill_voice
} ; !zone

; **************************************************

!zone {
snd_s_kill_all
    ldx #2
-
    jsr snd_s_kill_voice
    dex
    bpl -
    rts
; end sub snd_s_kill_all
} ; !zone

; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************

