; Top-hole Golf
; Copyright 2020 Matthew Clarke


; *****************
; *** CONSTANTS ***
; *****************
; Terminate list with 0.
interrupts_l_SPLITS !byte   $fa,0


; *****************
; *** VARIABLES ***
; *****************
interrupts_v_current_raster   !byte   0


; *******************
; ****** MACROS *****
; *******************


; *******************
; *** SUBROUTINES ***
; *******************
!zone {
interrupts_s_cb
    lda VICIRQ
    bpl interrupts_clean_up

    jsr splash_s_update

    jmp interrupts_reset
; end sub interrupts_s_cb
} ; !zone

; **************************************************

; NOTE: this is 'jmp'd to by the callback (cb) routines.
!zone {
interrupts_reset
    ldy interrupts_v_current_raster
    iny
-
    lda (INTERRUPTS_LO),y
    bne +
    ldy #0
    beq -   ; NOTE: force re-read!
+
    sty interrupts_v_current_raster
    sta RASTER
    +utils_m_clear_raster_bit9 
    ; Release latch.
    asl VICIRQ

; NOTE: will often jump straight to here.
interrupts_clean_up
    ; Pull registers off of stack and restore.
    pla
    tay
    pla
    tax
    pla
    rti

; end sub interrupts_reset
} ; !zone

; **************************************************

!zone {
interrupts_s_uninstall
    sei
    lda #0
    sta IRQMSK
    lda #<interrupts_empty_cb
    sta CINV
    lda #>interrupts_empty_cb
    sta CINV+1
    cli
    rts
; end sub interrupts_s_uninstall
} ; !zone

; **************************************************

!zone {
interrupts_empty_cb
    jmp interrupts_clean_up
; end sub interrupts_empty_cb
} ; !zone

; **************************************************

!zone {
interrupts_s_install
    sei

    lda #<interrupts_s_cb
    sta CINV
    lda #>interrupts_s_cb
    sta CINV+1
    lda #<interrupts_l_SPLITS
    sta INTERRUPTS_LO
    lda #>interrupts_l_SPLITS
    sta INTERRUPTS_HI

    ; Enable raster interrupts.
    lda #$01
    sta IRQMSK
    ; Turn off CIA interrupts.
    lda #$7f
    sta $dc0d
    
    ldy #0
    sty interrupts_v_current_raster
    lda (INTERRUPTS_LO),y
    sta RASTER
    +utils_m_clear_raster_bit9 

    cli
    rts

; end sub interrupts_s_install
} ; !zone

; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************

