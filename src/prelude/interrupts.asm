; Top-hole Golf
; Copyright 2020 Matthew Clarke


; *****************
; *** CONSTANTS ***
; *****************
; Terminate list with 0.
INTERRUPTS_TITLES_SPLITS    !byte   $db,0
INTERRUPTS_SETTINGS_SPLITS  !byte   $a4,$e6,0
INTERRUPTS_FADING_SPLITS    !byte   $fa,0
INTERRUPTS_SIGN_IN_SPLITS   !byte   $89,$fa,0


; *****************
; *** VARIABLES ***
; *****************
interrupts_current_raster   !byte   0


; *******************
; ****** MACROS *****
; *******************


; *******************
; *** SUBROUTINES ***
; *******************
!zone {
interrupts_cb_titles2
    lda VICIRQ
    bpl interrupts_clean_up
    jsr titles2_s_update
    jmp interrupts_reset
; end sub interrupts_cb_titles2
} ; !zone

; **************************************************

!zone {
interrupts_cb_settings
    lda VICIRQ
    bpl interrupts_clean_up

    lda interrupts_current_raster
    beq .lower_sprites
    jsr settings_s_update
    jmp interrupts_reset
.lower_sprites
    jsr settings_draw_lower_sprites
    jmp interrupts_reset

; end sub interrupts_cb_settings
} ; !zone

; **************************************************

!zone {
interrupts_cb_fading
    lda VICIRQ
    bpl interrupts_clean_up
    jsr fader_s_update
    jmp interrupts_reset

; end sub interrupts_cb_fading
} ; !zone

; **************************************************

!zone {
interrupts_s_cb_sign_in
    lda VICIRQ
    bpl interrupts_clean_up

    lda interrupts_current_raster
    beq .lower_sprites

    ; So update and draw upper sprites...
    jsr sign_s_update
    jmp interrupts_reset

.lower_sprites
    jsr sign_s_draw_lower_sprites
    jmp interrupts_reset

; end sub interrupts_s_cb_sign_in
} ; !zone

; **************************************************

; NOTE: this is 'jmp'd to by the callback (cb) routines.
!zone {
interrupts_reset
    jsr snd_s_update
    inc shared_v_random_seed

    ldy interrupts_current_raster
    iny
-
    lda (INTERRUPTS_LO),y
    bne +
    ldy #0
    beq -
+
    sty interrupts_current_raster
    sta RASTER
    +utils_m_clear_raster_bit9 
    ; Release latch.
    asl VICIRQ

; NOTE: will often jump straight to here.
interrupts_clean_up
    ; Pull registers off of stack and restore.
    pla
    tay
    pla
    tax
    pla
    rti

; end sub interrupts_reset
} ; !zone

; **************************************************

!zone {
interrupts_uninstall
    sei
    lda #0
    sta IRQMSK
    lda #<interrupts_empty_cb
    sta CINV
    lda #>interrupts_empty_cb
    sta CINV+1
    cli
    rts
; end sub interrupts_uninstall
} ; !zone

; **************************************************

!zone {
interrupts_empty_cb
    jmp interrupts_clean_up
; end sub interrupts_empty_cb
} ; !zone

; **************************************************

!zone {
interrupts_install
    sei

    lda prelude_v_current_mode
    cmp #prelude_c_MODE_TITLES
    beq .titles
    cmp #prelude_c_MODE_SIGN_IN
    beq .sign_in
    cmp #prelude_c_MODE_FADING
    beq .fading

    ; So, settings...
    lda #<interrupts_cb_settings
    sta CINV
    lda #>interrupts_cb_settings
    sta CINV+1
    lda #<INTERRUPTS_SETTINGS_SPLITS
    sta INTERRUPTS_LO
    lda #>INTERRUPTS_SETTINGS_SPLITS
    sta INTERRUPTS_HI
    jmp .enable

.titles
    lda #<interrupts_cb_titles2
    sta CINV
    lda #>interrupts_cb_titles2
    sta CINV+1
    lda #<INTERRUPTS_TITLES_SPLITS
    sta INTERRUPTS_LO
    lda #>INTERRUPTS_TITLES_SPLITS
    sta INTERRUPTS_HI
    jmp .enable

.fading
    lda #<interrupts_cb_fading
    sta CINV
    lda #>interrupts_cb_fading
    sta CINV+1
    lda #<INTERRUPTS_FADING_SPLITS
    sta INTERRUPTS_LO
    lda #>INTERRUPTS_FADING_SPLITS
    sta INTERRUPTS_HI
    jmp .enable

.sign_in
    lda #<interrupts_s_cb_sign_in
    sta CINV
    lda #>interrupts_s_cb_sign_in
    sta CINV+1
    lda #<INTERRUPTS_SIGN_IN_SPLITS
    sta INTERRUPTS_LO
    lda #>INTERRUPTS_SIGN_IN_SPLITS
    sta INTERRUPTS_HI

.enable
    ; Enable raster interrupts.
    lda #$01
    sta IRQMSK
    ; Turn off CIA interrupts.
    lda #$7f
    sta $dc0d
    
    ldy #0
    sty interrupts_current_raster
    lda (INTERRUPTS_LO),y
    sta RASTER
    +utils_m_clear_raster_bit9 

    cli
    rts

; end sub interrupts_install
} ; !zone

; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************

