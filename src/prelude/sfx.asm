; Top-hole Golf
; Copyright 2020 Matthew Clarke


; *****************
; *** CONSTANTS ***
; *****************
; Data tables for sound effects:
; FL,FH,PL,PH,[placeholder],AD,SR,WV,<frames>, 
;   FILTER-MODE,RESONANCE,CUTOFF-LO,CUTOFF-HI,
;   <must loop>
; Initial value of $ff means end sound effect.
sfx_l_BROWSE2_INIT  !byte   195,16, 0,0, 0, $22,$f2, $11, 2, 0,0,0,0, 0
sfx_l_BROWSE2_DATA  !byte   30,25,2, $ff,$10
sfx_l_INVALID_INIT  !byte   195,16, 0,0, 0, $22,$f2, $11, 2, 0,0,0,0, 0
sfx_l_INVALID_DATA  !byte   195,17,2, $ff,$10
sfx_l_SELECT_INIT   !byte   135,33, 0,0, 0, $22,$f2, $11, 2, 0,0,0,0, 0
sfx_l_SELECT_DATA   !byte   162,37,2, 62,42,2, 60,50,2, 15,67,2, $ff,$10
sfx_l_TYPE_INIT     !byte   99,56, 0,0, 0, $11,$f2, $11, 2, 0,0,0,0, 0
sfx_l_TYPE_DATA     !byte   $ff,$10
sfx_l_DELETE_INIT   !byte   49,28, 0,0, 0, $11,$f2, $11, 2, 0,0,0,0, 0
sfx_l_DELETE_DATA   !byte   $ff,$10
sfx_l_BACK_INIT     !byte   195,16, 0,0, 0, $11,$f9, $11, 4, 0,0,0,0, 0
sfx_l_BACK_DATA     !byte   30,25,2, $ff,$10
sfx_l_DELETE_PLAYER_INIT    !byte   135,33, 0,0, 0, $11,$f8, $11, 2, 0,0,0,0, 0
sfx_l_DELETE_PLAYER_DATA    !byte   30,25,2, 195,16,2, $ff,$10

sfx_c_BROWSE2   = 0
sfx_c_SELECT    = 1
sfx_c_TYPE      = 2
sfx_c_DELETE    = 3
sfx_c_INVALID   = 4
sfx_c_BACK      = 5
sfx_c_DELETE_PLAYER = 6

sfx_l_INIT_ADDR_LO
    !byte   <sfx_l_BROWSE2_INIT
    !byte   <sfx_l_SELECT_INIT
    !byte   <sfx_l_TYPE_INIT
    !byte   <sfx_l_DELETE_INIT
    !byte   <sfx_l_INVALID_INIT
    !byte   <sfx_l_BACK_INIT
    !byte   <sfx_l_DELETE_PLAYER_INIT
sfx_l_INIT_ADDR_HI
    !byte   >sfx_l_BROWSE2_INIT
    !byte   >sfx_l_SELECT_INIT
    !byte   >sfx_l_TYPE_INIT
    !byte   >sfx_l_DELETE_INIT
    !byte   >sfx_l_INVALID_INIT
    !byte   >sfx_l_BACK_INIT
    !byte   >sfx_l_DELETE_PLAYER_INIT
sfx_l_DATA_ADDR_LO
    !byte   <sfx_l_BROWSE2_DATA
    !byte   <sfx_l_SELECT_DATA
    !byte   <sfx_l_TYPE_DATA
    !byte   <sfx_l_DELETE_DATA
    !byte   <sfx_l_INVALID_DATA
    !byte   <sfx_l_BACK_DATA
    !byte   <sfx_l_DELETE_PLAYER_DATA
sfx_l_DATA_ADDR_HI
    !byte   >sfx_l_BROWSE2_DATA
    !byte   >sfx_l_SELECT_DATA
    !byte   >sfx_l_TYPE_DATA
    !byte   >sfx_l_DELETE_DATA
    !byte   >sfx_l_INVALID_DATA
    !byte   >sfx_l_BACK_DATA
    !byte   >sfx_l_DELETE_PLAYER_DATA


; *****************
; *** VARIABLES ***
; *****************


; *******************
; ****** MACROS *****
; *******************


; *******************
; *** SUBROUTINES ***
; *******************

; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************

