; Top-hole Golf
; Copyright 2020 Matthew Clarke

; *****************
; *** CONSTANTS ***
; *****************
; 8 slots to use for subroutine parameters (or return values).
P0  =   $02
P1  =   $03
P2  =   $04
P3  =   $05
P4  =   $06
P5  =   $07
P6  =   $08
P7  =   $09

; 8 slots for use inside maths routines.
MATHS0  =   $0a
MATHS1  =   $0b
MATHS2  =   $0c
MATHS3  =   $0d
MATHS4  =   $0e
MATHS5  =   $0f
MATHS6  =   $10
MATHS7  =   $11

TREES_LO = $12
TREES_HI = $13

CAMERA0 =   $16
CAMERA1 =   $17
CAMERA2 =   $18
CAMERA3 =   $19

BITMAP_LO   =   $1a
BITMAP_HI   =   $1b
PATTERN_LO  =   $1c
PATTERN_HI  =   $1d
VM_LO       =   $1e
VM_HI       =   $1f
COLORS_LO   =   $2a
COLORS_HI   =   $2b

LINE_X0_LO = $2c
LINE_Y0_LO = $2d
LINE_X1_LO = $2e
LINE_Y1_LO = $2f

EDGES_LO = $30
EDGES_HI = $31

; Use this with the 'wind' and 'slope' modules.
WS_X_LO = $32
WS_X_HI = $33
WS_Z_LO = $34
WS_Z_HI = $35

; Copies of P0-P7.  Some interrupt routines make a copy of the originals and
; then restore them so that main thread is reentrant...
PP0 = $36
PP1 = $37
PP2 = $38
PP3 = $39
PP4 = $3a
PP5 = $3b
PP6 = $3c
PP7 = $3d
MMATHS0 = $3e
MMATHS1 = $3f
MMATHS2 = $40
MMATHS3 = $41
MMATHS4 = $42
MMATHS5 = $43
MMATHS6 = $44
MMATHS7 = $45

PARTSYS_LO = $46
PARTSYS_HI = $47

ROT0 = $48
ROT1 = $49
ROT2 = $4a
ROT3 = $4b
ROT4 = $4c
ROT5 = $4d
ROT6 = $4e
ROT7 = $4f
ROT8 = $50

BALL_TRI_X_LO = $53
BALL_TRI_X_HI = $54
BALL_TRI_Z_LO = $55
BALL_TRI_Z_HI = $56

POWARC_LO = $59
POWARC_HI = $5a
POWARC_FILL_SRC_ITER_LO = $5b
POWARC_FILL_SRC_ITER_HI = $5c
POWARC_COPY_LO = $5d
POWARC_COPY_HI = $5e

INTERRUPTS_LO = $5f
INTERRUPTS_HI = $60

; Exclusively for use with 16-bit multiplication/division routines.
SIGN_CHANGED = $63
SIGN_BIT_MASK = $64

; For exclusive use of 'titles2' module (fading stuff).
FADE_CR_LO = $65
FADE_CR_HI = $66
FADE_SR_LO = $67
FADE_SR_HI = $68
FADE_CR_SRC_LO = $69
FADE_CR_SRC_HI = $6a
FADE_SR_SRC_LO = $6b
FADE_SR_SRC_HI = $6c

; When signing in (editing name).
CURSOR_POS_LO = $6d
CURSOR_POS_HI = $6e
CURSOR_POS_SR_LO = $6f
CURSOR_POS_SR_HI = $70

; Used when drawing (infinitely distant) b/g objects.
BDROP_OBJ_OFFSET = $71
BDROP_OBJ_IS_LEADING = $72

DEBUG_DUMP_LO = $73
DEBUG_DUMP_HI = $74

SND_CH1_DATA_ZP_LO = 75
SND_CH1_DATA_ZP_HI = 76
SND_CH2_DATA_ZP_LO = 77
SND_CH2_DATA_ZP_HI = 78
SND_CH3_DATA_ZP_LO = 79
SND_CH3_DATA_ZP_HI = 80
; Temporary zp store for 'init' data.
SND_INIT_DATA_ZP_LO = 81
SND_INIT_DATA_ZP_HI = 82
; Temporary zp store for SID regs base address.
SND_REGS_BASE_ZP_LO = 83
SND_REGS_BASE_ZP_HI = 84

RANDOM_X_TMP = 85

; Reserved for use by SCNKEY (Kernal's keyboard scan routine).
SFDX = $cb


; *****************
; *** VARIABLES ***
; *****************


; *******************
; ****** MACROS *****
; *******************


; *******************
; *** SUBROUTINES ***
; *******************

; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************

