; Top-hole Golf
; Copyright 2020 Matthew Clarke

; *****************
; *** CONSTANTS ***
; *****************
app_c_MODE_START_TITLES         =   0
app_c_MODE_TITLES               =   1
app_c_MODE_START_PLAY           =   2
app_c_MODE_PLAY                 =   3
app_c_MODE_START_SCORE_CARDS    =   4
app_c_MODE_SCORE_CARDS          =   5
app_c_MODE_RESUME_PLAY          =   6
app_c_MODE_START_SETTINGS       =   7
app_c_MODE_SETTINGS             =   8
app_c_MODE_SPLASH               = 9


; *****************
; *** VARIABLES ***
; *****************
app_v_current_mode !byte   0
app_v_filename  !pet    "prelude.prg",0
app_v_filename2 !pet    "play.prg",0
app_v_filename3 !pet    "splash.prg",0


; *******************
; ****** MACROS *****
; *******************


; *******************
; *** SUBROUTINES ***
; *******************
!zone {
app_s_init
    jsr app_s_run_splash
    jsr app_s_run_prelude

    ldx #<app_v_filename2 
    ldy #>app_v_filename2 
    jsr CB_LOADFILE
    
    jmp ($2000)

    rts
; end sub app_s_init
} ; !zone

; **************************************************

!zone {
app_s_run_splash
    ldx #<app_v_filename3
    ldy #>app_v_filename3
    jsr CB_LOADFILE
    jmp ($2000)
    rts
; end sub app_s_run_splash
} ; !zone

; **************************************************

!zone {
app_s_run_prelude
    ldx #<app_v_filename  
    ldy #>app_v_filename  
    jsr CB_LOADFILE
    ; File is loaded to $2000.
    ; First two bytes are exe address - points to an 'init' routine.
    ; When that routine does an rts, CPU will come back here.
    ; FIXME: BUG: sometimes this doesn't work - we just get stuck inside
    ; interrupts and main prelude loop never executes.
    jmp ($2000)
    rts
; end sub app_s_run_prelude
} ; !zone

; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************

