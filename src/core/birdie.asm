; Top-hole Golf
; Copyright 2020 Matthew Clarke

!to "birdie.o",cbm
!source "vic_ii.asm"
!source "zeropage.asm"
!source "screen_codes.asm"
!source "sprite_data.asm"
!source "mymacros.asm"

*= $600
!bin "multiload.bin"

*= $c00
    jsr CB_INITLOADER

    ; Turn off BASIC ROM.
    lda R6510
    and #$fe
    sta R6510

    lda #BLACK
    sta EXTCOL
    sta BGCOL0
    jsr gfxs_s_init

    jsr app_s_init

!source "joystick.asm"
!source "utilities.asm"
!source "gfx_setup.asm"
!source "app.asm"
!source "sprite_engine.asm"
!source "draw_primitives.asm"
!source "shared_data.asm"
!source "mc_bitmap_font.asm"
!source "icons.asm"
!source "messaging.asm"
wheres_here


