; Top-hole Golf
; Copyright 2020 Matthew Clarke


powarc_c_BEGIN = *


; *****************
; *** CONSTANTS ***
; *****************
; Where we start drawing from.
powarc_c_TOP_ROW = 17

POWARC_BASE_ADDR = gfxs_c_BITMAP_BASE+18*8*40+8
POWARC_POWER_STEPS_DATA
    !bin "../../assets/tables/powsteps.bin"
POWARC_PRECISION_STEPS_DATA
    !bin "../../assets/tables/powarc_precision_steps3.bin"
POWARC_STEP_THRESHOLD = 3
POWARC_POWERING_UP_NUM_STEPS = 36

POWARC_BITMAP_COPY_ROWS_LO
    !for i,48 {
        !byte <(powarc_v_bitmap_copy+(((i-1)/8)*48)+((i-1)%8)-8)
    } ; !for
POWARC_BITMAP_COPY_ROWS_HI
    !for i,48 {
        !byte >(powarc_v_bitmap_copy+(((i-1)/8)*48)+((i-1)%8)-8)
    } ; !for

powarc_c_PRECISION_DELTA = 40
powarc_c_INITIAL_DELAY_LO = 50
powarc_c_INITIAL_DELAY_HI = 5

powarc_l_BASE_BITMAP_ICON
    !bin    "../../assets/patterns/powarcbase.bin"

powarc_c_FULL_POWER_OFFSET = 31
powarc_l_POWER_REDUCTIONS = *-32
    !byte   24,22,20,18,16

powarc_c_PRECISION_BEGIN    = 12
powarc_c_PRECISION_CENTER   = 20
powarc_c_PRECISION_END      = 28
powarc_c_PRECISION_BUNKER_BEGIN = 16
powarc_c_PRECISION_BUNKER_END   = 24

powarc_c_MAX_SW_SPR_NUM = 15
powarc_c_MAX_HW_SPR_NUM = ball_c_SHADOW_SW_SPR_NUM 
powarc_c_MAX_SPR_X = 19+spr_c_VISIBLE_ALL_L 
powarc_c_MAX_SPR_Y = 139+spr_c_VISIBLE_ALL_T
powarc_c_MAX_SPR_PTR = (play_l_MAX_STR_SPRITE-$c000)/64
powarc_c_MAX_ANIMATION_FRAME_RATE = 5
powarc_l_MAX_ANIM_COLOR_CYCLE   !byte WHITE,YELLOW,ORANGE,RED,ORANGE,YELLOW
                                !byte WHITE,LIGHT_BLUE,(-1)


; *****************
; *** VARIABLES ***
; *****************
powarc_frame_count  !byte   0

; NOTE: keep these six variables together because they're cleared in a loop!
powarc_iter         !byte   0
powarc_v_step_count   !byte   0
powarc_v_power_offset   !byte   0
; NOTE: keep hold of this value for an easy test to see if spin was in a valid
; range for bunker shot...
powarc_v_precision_offset !byte   0
powarc_v_power_maxed_out    !byte   0
powarc_v_precision_ended    !byte   0

powarc_v_bitmap_copy  !fill   (6*6*8),0

powarc_v_precision_delay_lo !byte   0
powarc_v_precision_delay_hi !byte   0
powarc_v_min_delay_reached  !byte   0

powarc_v_max_animation_active   !byte   0
powarc_v_max_anim_frame_count   !byte   0
powarc_v_max_anim_iter          !byte   0


; *******************
; ****** MACROS *****
; *******************


; *******************
; *** SUBROUTINES ***
; *******************
!zone {
powarc_s_update
    lda golfer_v_current_state
    cmp #golfer_c_STATE_POWERING_UP
    beq .powering_up
    cmp #golfer_c_STATE_PRECISION
    beq .precision

    ; FIXME: why is this routine being called at all if not in middle of 
    ; golf swing?!  (Answer: holding?)
    bne .end

.powering_up
    jsr powarc_s_advance_power
    rts ; EXIT POINT.

.precision
    lda powarc_v_min_delay_reached 
    +branch_if_true +

    ; We haven't yet reached a delay of one frame, so keep subtracting...
    lda powarc_v_precision_delay_lo 
    sec
    sbc #powarc_c_PRECISION_DELTA 
    sta powarc_v_precision_delay_lo 
    lda powarc_v_precision_delay_hi 
    sbc #0
    sta powarc_v_precision_delay_hi 
    bne +
    inc powarc_v_precision_delay_hi
    inc powarc_v_min_delay_reached
+
    ; Time to advance marker (and go to next step)?
    dec powarc_frame_count
    bne .end
    lda powarc_v_precision_delay_hi
    sta powarc_frame_count
    jsr powarc_s_advance_precision

.end
    rts
; end sub powarc_s_update
} ; !zone

; **************************************************

!zone {
powarc_s_reset
    ; NOTE: set 'frame_count' to 1 initially so that at least one segment of
    ; the power-arc is always filled in, no matter how briefly the player
    ; holds down the fire button.
    lda #1
    sta powarc_frame_count
    lda #0
    sta powarc_v_max_animation_active   
    tax
-
    sta powarc_iter,x
    inx
    cpx #(powarc_v_precision_ended-powarc_iter+1)
    bne -

    ; NOTE: POWARC_FILL_SRC_ITER_LO/HI are zero-page variables.
    lda #<POWARC_POWER_STEPS_DATA
    sta POWARC_FILL_SRC_ITER_LO 
    lda #>POWARC_POWER_STEPS_DATA
    sta POWARC_FILL_SRC_ITER_HI 

    lda #<powarc_l_BASE_BITMAP_ICON 
    sta P0
    lda #>powarc_l_BASE_BITMAP_ICON 
    sta P1
    lda #$ff
    sta P2
    jsr icon_s_draw

    ; Make off-screen copy of bitmap data.
    ; 6*6*8 = 288 bytes.  Must skip over 5-byte header.
    ldx #5
-
    lda powarc_l_BASE_BITMAP_ICON,x
    sta powarc_v_bitmap_copy-5,x  
    lda powarc_l_BASE_BITMAP_ICON+144,x
    sta powarc_v_bitmap_copy+144-5,x
    inx
    cpx #149
    bne -

    rts
; end sub powarc_s_reset
} ; !zone

; **************************************************

!zone {
powarc_s_advance_power
    ldy #0
    
.loop_top
    ; First the row.  If it's 0, zp is already correctly set.  If it's $ff,
    ; the 'step' is complete.
    lda (POWARC_FILL_SRC_ITER_LO),y
    beq .offset
    cmp #$ff
    beq .end
    ; Transfer row number from A to X and set up zero page.
    tax
    lda dp_l_BITMAP_ROWS_LO,x
    sta POWARC_LO 
    lda dp_l_BITMAP_ROWS_HI,x
    sta POWARC_HI 
    ; ... and same for the off-screen copy.
    lda POWARC_BITMAP_COPY_ROWS_LO-(powarc_c_TOP_ROW*8),x
    sta POWARC_COPY_LO 
    lda POWARC_BITMAP_COPY_ROWS_HI-(powarc_c_TOP_ROW*8),x
    sta POWARC_COPY_HI 

.offset
    iny
    lda (POWARC_FILL_SRC_ITER_LO),y
    ; Store offset temporarily (- i.e. offset from beginning of current row).
    sta MATHS0

    ; Now get the pattern and draw it.
    iny
    ; Need to store iterator temporarily.
    sty MATHS1
    lda (POWARC_FILL_SRC_ITER_LO),y
    ; Load offset into Y.
    ldy MATHS0
    sta (POWARC_LO),y
    sta (POWARC_COPY_LO),y

    ; Load iterator back into Y.
    ldy MATHS1
    iny
    jmp .loop_top

.end

    iny
    ; Add this offset to z.p. pointer, ready for next time.
    tya
    clc
    adc POWARC_FILL_SRC_ITER_LO
    sta POWARC_FILL_SRC_ITER_LO
    lda POWARC_FILL_SRC_ITER_HI
    adc #0
    sta POWARC_FILL_SRC_ITER_HI

    inc powarc_v_step_count
    lda powarc_v_step_count
    cmp #POWARC_POWERING_UP_NUM_STEPS
    bne +
    ; Already at max power so set flag.
    sta powarc_v_power_maxed_out
+
    rts
; end sub powarc_s_advance_power
} ; !zone

; **************************************************

!zone {
.ITER = MATHS1

powarc_s_advance_precision
    ldy #0
.loop_erase
    ; Get the row.  Zero means the same row as last time; $ff means this
    ; section has ended.
    lda (POWARC_FILL_SRC_ITER_LO),y
    beq .offset
    cmp #$ff
    beq .draw
    ; Must set z.p. pointers for destination.
    tax
    lda dp_l_BITMAP_ROWS_LO,x
    sta POWARC_LO 
    lda dp_l_BITMAP_ROWS_HI,x
    sta POWARC_HI 
    ; Use copy as source (cf. masking).
    lda POWARC_BITMAP_COPY_ROWS_LO-(powarc_c_TOP_ROW*8),x
    sta POWARC_COPY_LO
    lda POWARC_BITMAP_COPY_ROWS_HI-(powarc_c_TOP_ROW*8),x
    sta POWARC_COPY_HI

.offset
    iny
    ; Store offset temporarily in MATHS0.
    lda (POWARC_FILL_SRC_ITER_LO),y
    sta MATHS0
    ; Get the mask.
    iny
    lda (POWARC_FILL_SRC_ITER_LO),y
    ; Put iterator temporarily in MATHS1.
    sty .ITER
    ldy MATHS0
    ; Now we have mask in A and offset in Y.
    ; AND with bitmap and then write the result.
    and (POWARC_COPY_LO),y
    sta (POWARC_LO),y
    ; Skip over 'pattern', then increment again so ready for next entry.
    ; Restore iterator into Y.
    ldy .ITER
    iny
    iny
    ; This is the 'restore' pattern:
    ; FIXME: is an AND required here?!
    lda (POWARC_FILL_SRC_ITER_LO),y
    sty .ITER
    ldy MATHS0
    ora (POWARC_COPY_LO),y
    sta (POWARC_LO),y

    ldy .ITER
    iny
    jmp .loop_erase
    
.draw
    ; Add Y+1 to the source pointer.  This is what we'll erase next time
    ; round.
    iny
    tya
    clc
    adc POWARC_FILL_SRC_ITER_LO
    sta POWARC_FILL_SRC_ITER_LO
    lda POWARC_FILL_SRC_ITER_HI
    adc #0
    sta POWARC_FILL_SRC_ITER_HI

    ; The z-p address pointer has now been reset, so begin to index from
    ; 0 again...
    ldy #0
.loop_draw
    lda (POWARC_FILL_SRC_ITER_LO),y
    beq .offset2
    cmp #$ff
    beq .end
    tax
    lda dp_l_BITMAP_ROWS_LO,x
    sta POWARC_LO 
    lda dp_l_BITMAP_ROWS_HI,x
    sta POWARC_HI 
    lda POWARC_BITMAP_COPY_ROWS_LO-(powarc_c_TOP_ROW*8),x
    sta POWARC_COPY_LO
    lda POWARC_BITMAP_COPY_ROWS_HI-(powarc_c_TOP_ROW*8),x
    sta POWARC_COPY_HI
.offset2
    iny
    lda (POWARC_FILL_SRC_ITER_LO),y
    sta MATHS0
    ; Get pattern.  Remember to skip over mask!
    iny
    iny
    lda (POWARC_FILL_SRC_ITER_LO),y
    sty .ITER
    ldy MATHS0
    ora (POWARC_COPY_LO),y
    sta (POWARC_LO),y

    ; Restore iterator, advance to next entry & go again.
    ldy .ITER
    iny
    iny
    jmp .loop_draw

.end
    ; See if next byte in table is $fe.  If it is, we're at the end of
    ; the table and won't be able to process it any further.
    inc powarc_v_step_count
    iny
    lda (POWARC_FILL_SRC_ITER_LO),y
    cmp #$fe
    bne +
    sta powarc_v_precision_ended 
+
    rts
; end sub powarc_s_advance_precision
} ; !zone

; **************************************************

!zone {
powarc_s_init_precision
    lda #0
    sta powarc_iter
    sta powarc_v_step_count
    sta powarc_v_min_delay_reached

    lda #powarc_c_INITIAL_DELAY_LO 
    sta powarc_v_precision_delay_lo 
    lda #powarc_c_INITIAL_DELAY_HI 
    sta powarc_v_precision_delay_hi 
    sta powarc_frame_count

    lda #<POWARC_PRECISION_STEPS_DATA
    sta POWARC_FILL_SRC_ITER_LO 
    lda #>POWARC_PRECISION_STEPS_DATA
    sta POWARC_FILL_SRC_ITER_HI 

    rts
; end sub powarc_s_init_precision
} ; !zone

; **************************************************

; INPUTS:   A = value of 'powarc_v_power_offset'
!zone {
powarc_s_init_max_animation
    ; We'll go through with this only if at max power!
    cmp #powarc_c_FULL_POWER_OFFSET
    beq +
    rts ; EXIT POINT.

+
    lda #<powarc_c_MAX_SPR_X
    sta spr_v_x_lo+powarc_c_MAX_SW_SPR_NUM
    lda #>powarc_c_MAX_SPR_X
    sta spr_v_x_hi+powarc_c_MAX_SW_SPR_NUM
    lda #powarc_c_MAX_SPR_Y
    sta spr_v_y+powarc_c_MAX_SW_SPR_NUM
    lda #powarc_c_MAX_SPR_PTR
    sta spr_v_current_ptr+powarc_c_MAX_SW_SPR_NUM
    lda #WHITE
    sta spr_v_color+powarc_c_MAX_SW_SPR_NUM
    sta spr_v_hires+powarc_c_MAX_SW_SPR_NUM
    sta spr_v_xxpand+powarc_c_MAX_SW_SPR_NUM 
    sta powarc_v_max_animation_active

    lda #powarc_c_MAX_ANIMATION_FRAME_RATE
    sta powarc_v_max_anim_frame_count
    lda #0
    sta powarc_v_max_anim_iter

    ldy #sfx_c_MAX_POWER
    jsr snd_s_init_sfx

    rts
; end sub powarc_s_init_max_animation
} ; !zone

; **************************************************

!zone {
powarc_s_update_max_animation
    lda powarc_v_max_animation_active
    +branch_if_false .end

    dec powarc_v_max_anim_frame_count
    bne .end
    ldx powarc_v_max_anim_iter
    inx
    lda powarc_l_MAX_ANIM_COLOR_CYCLE,x
    bpl +

    ; Animation has ended.
    lda #0
    sta powarc_v_max_animation_active

    ; FIXME: hack!
    ; Push h/w sprite offscreen to hide it.
    lda #spr_c_OFFSCREEN_B 
    sta SP0Y+2*powarc_c_MAX_HW_SPR_NUM

    rts ; EXIT POINT.

+
    sta spr_v_color+powarc_c_MAX_SW_SPR_NUM
    stx powarc_v_max_anim_iter
    lda #powarc_c_MAX_ANIMATION_FRAME_RATE
    sta powarc_v_max_anim_frame_count

.end
    rts
; end sub powarc_s_update_max_animation
} ; !zone

; **************************************************

!zone {
powarc_s_draw_max_animation
    lda powarc_v_max_animation_active
    +branch_if_false .end

    ldy #powarc_c_MAX_SW_SPR_NUM
    ldx #powarc_c_MAX_HW_SPR_NUM
    jsr spr_s_write_to_vic_ii

.end
    rts
; end sub powarc_s_draw_max_animation
} ; !zone

; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************

powarc_c_SIZE = *-powarc_c_BEGIN

