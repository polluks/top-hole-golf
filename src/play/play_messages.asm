; Top-hole Golf
; Copyright 2020 Matthew Clarke


playmsg_c_BEGIN = *


; *****************
; *** CONSTANTS ***
; *****************
playmsg_c_ADJECTIVE_STR !raw "Nice",0,"Good",0,"Great",0,"Super",0
playmsg_c_SHOT_TYPE_STR !raw "Double bogey",0,"Bogey",0,"par",0,"birdie",0,"eagle",0,"putt",0
playmsg_c_NUM_ADJECTIVES = 4

; NOTE: order these so that they correspond to (par - shots).
playmsg_c_TYPE_DOUBLE_BOGEY = 0
playmsg_c_TYPE_BOGEY        = 1
playmsg_c_TYPE_PAR          = 2
playmsg_c_TYPE_BIRDIE       = 3
playmsg_c_TYPE_EAGLE        = 4
playmsg_c_TYPE_PUTT         = 5

playmsg_l_ADJ_OFFSETS   !byte   0,5,10,16
playmsg_l_TYPE_OFFSETS  !byte   0,13,19,23,30,36

playmsg_c_BUFFER_LEN = 13


; *****************
; *** VARIABLES ***
; *****************
playmsg_v_buffer    !fill   5+6+2


; *******************
; ****** MACROS *****
; *******************


; *******************
; *** SUBROUTINES ***
; *******************
; INPUTS: X = shot type.
!zone {
.SHOT_TYPE = MATHS0

playmsg_s_praise_shot
    stx .SHOT_TYPE    
    jsr playmsg_s_clear_buffer

    ; NOTE: that routine above didn't alter X!
    cpx #playmsg_c_TYPE_PAR
    bcs +
    ; We're going to skip the adjective.
    ldy #0
    beq .skip_adjective

+
    ; Pick a random adjective.
    ; FIXME: separate routine in random module?!
    jsr rand_s_get
    and #$03

    ; Put adjective offset into X; Y will keep track of destination offset.
    tax
    lda playmsg_l_ADJ_OFFSETS,x
    tax
    ldy #0
    ; Copy adjective into buffer.
-
    lda playmsg_c_ADJECTIVE_STR,x
    beq +
    sta playmsg_v_buffer,y
    iny
    inx
    bne -

+
    ; Advance destination offset by one space, ready for shot type.
    ; Offset of shot type string goes into X.
    iny
.skip_adjective
    ldx .SHOT_TYPE
    lda playmsg_l_TYPE_OFFSETS,x
    tax
    ; Copy shot type into buffer.
-
    lda playmsg_c_SHOT_TYPE_STR,x
    beq .end_type
    sta playmsg_v_buffer,y
    iny
    inx
    bne -

.end_type
    ; And finally an exclamation mark.
    lda #SCR_CODE_BANG   
    sta playmsg_v_buffer,y

;    ; FIXME: check for bug where incorrect message is displayed (no noun)!
;    lda playmsg_v_buffer-1,y
;    cmp #$20
;    bne +
;-
;    inc EXTCOL
;    jmp -

+
    lda #<playmsg_v_buffer
    sta P0
    lda #>playmsg_v_buffer
    sta P1
    lda #playmsg_c_BUFFER_LEN
    sta P4
    jsr msg_s_display

    rts
; end sub playmsg_s_praise_shot
} ; !zone

; **************************************************

!zone {
playmsg_s_clear_buffer
    ldy #playmsg_c_BUFFER_LEN-1
    lda #SCR_CODE_SPACE  
-
    sta playmsg_v_buffer,y
    dey
    bpl -

    rts
; end sub playmsg_s_clear_buffer
} ; !zone

; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************

playmsg_c_SIZE = *-playmsg_c_BEGIN 

