; Top-hole Golf
; Copyright 2020 Matthew Clarke


round_c_BEGIN = *


; *****************
; *** CONSTANTS ***
; *****************
; State machine.
ROUND_STATE_LOADING_HOLE            =   0
ROUND_STATE_ORDERING_PLAYERS        =   1
ROUND_STATE_DRAWING_STATS_DISPLAY   =   2
ROUND_STATE_ROTATING_WORLD          =   3
ROUND_STATE_RENDERING_3D_SCENE      =   4
; NOTE: synchronize this one with raster:
ROUND_STATE_SHOT_IN_PROGRESS        =   5
ROUND_STATE_SHOT_COMPLETE           =   6
ROUND_STATE_WAITING_FOR_NEXT        =   7
ROUND_STATE_DRAWING_SCORE_CARDS     =   8
round_c_STATE_WIPING_SCREEN   = 9

ROUND_NUM_HOLES = 18
; FIXME: for debugging purposes!
ROUND_HOLES_TO_PLAY = 9

round_c_MAX_PUTT_DISTANCE_LO = <(20*hole_c_PIXELS_PER_YARD)
round_c_MAX_PUTT_DISTANCE_HI = >(20*hole_c_PIXELS_PER_YARD)

ROUND_SKY_COLORS_N = 2

round_c_LOADING_MSG !raw    "Loading hole ",0,0
round_c_LOADING_MSG_LEN = 15

; Index into these two tables is 'shared_v_holes'.
; Cf. 0 = 18, 1 = front nine, 2 = back nine.
round_l_FIRST_HOLE_TO_PLAY          !byte   0,0,9
round_l_LAST_HOLE_TO_PLAY_PLUS_ONE  !byte   18,9,18


; *****************
; *** VARIABLES ***
; *****************
; Count from 0 to 17.
round_v_current_hole      !byte   0
round_current_state     !byte   0
round_v_current_player    !byte   0
; NOTE: this updated after each shot (inside 'golfer_record_distance').
; It's then used by 'ball_s_display_distance_of_shot' to report on current lie.
round_v_current_player_is_on_green  !byte   0
; NOTE: at the start of each hole, we don't refresh the playing order between
; shots.  First, everyone tees off, either in the order they signed in (for
; hole #1) or according to their score (hole #2 onwards) - best player first.
; The variable 'round_current_tee_index' keeps track of where we are in that
; initial teeing-off stage (- use it as an index into the
; 'players_playing_order' array).
round_v_teeing_off        !byte   0
round_current_tee_index !byte   0
round_v_must_putt         !byte   0
round_must_render       !byte   0
; Use this to find the correct sprite data.
round_current_gender_offset !byte   0
round_can_change_club       !byte   0
round_border_color          !byte   BLACK
round_camera_shake_enabled  !byte   0
round_sky_color_iter        !byte   0
; For your convenience:
round_current_sky_color     !byte   CYAN
round_v_current_ground_color    !byte   ORANGE

; FIXME: temporary code!!!
round_v_hole_filename   !pet    "h0000.prg",0


; *******************
; ****** MACROS *****
; *******************


; *******************
; *** SUBROUTINES ***
; *******************
!zone {
round_init
    jsr gfxs_s_init_colors
    jsr sc_s_reset_all
    jsr golfer_init2
    jsr sstore_init
    jsr players_s_reset

    lda #0
    sta round_v_current_player
    ldx shared_v_holes
    lda round_l_FIRST_HOLE_TO_PLAY,x
    sta round_v_current_hole

    lda #round_c_STATE_WIPING_SCREEN   
    sta round_current_state
    lda #0
    sta transtn_v_is_active
    sta round_current_sky_color
    sta round_v_current_ground_color

    lda #1
    sta round_must_render
    lda round_border_color
    sta EXTCOL

    jsr msg_s_clear

    rts
; end sub round_init
} ; !zone

; **************************************************

!zone {
.OFFSET_TO_QUADS = 18

round_s_load_hole
    ; Is the round complete?
    ldx shared_v_holes
    lda round_v_current_hole
    cmp round_l_LAST_HOLE_TO_PLAY_PLUS_ONE,x
    bne +
    ; YES, round is complete!!!
-   inc EXTCOL
    jmp -

    ; Turn the current hole number (counting from 0) into decimal digits.
+
    sta P0
    lda #0
    sta P1
    jsr utils_s_16bit_hex_to_dec
    ; Convert these digits into PETSCII (same as ASCII for '0' to '9') and
    ; write those bytes directly into the template filename.
    ; Now we have the filename that we want to load.
    lda utils_v_dec_digits    
    clc
    adc #font_c_ASCII_0 
    sta round_v_hole_filename+4
    lda utils_v_dec_digits+1    
    clc
    adc #font_c_ASCII_0 
    sta round_v_hole_filename+3
    ; And we must do the same again for the course index.
    lda shared_v_course_index

    ; FIXME: only one course at the moment!
    lda #1

    sta P0
    lda #0
    sta P1
    jsr utils_s_16bit_hex_to_dec
    lda utils_v_dec_digits    
    clc
    adc #font_c_ASCII_0 
    sta round_v_hole_filename+2
    lda utils_v_dec_digits+1    
    clc
    adc #font_c_ASCII_0 
    sta round_v_hole_filename+1

    ldx #<round_v_hole_filename
    ldy #>round_v_hole_filename
    jsr CB_LOADFILE
    ; Turn SuperCPU back on, if present.  (Multiload routines turn it off!)
    +utils_m_turn_on_supercpu

    lda #$00
    sta P0
    lda #$e0
    sta P1

    ; Start loading from $e000-$ffff.
    sei
    lda R6510
    and #%11111101
    sta R6510

    ; NOTE: input to target_init is the address held in P0/P1.  It records
    ; the first four bytes for the position of the target.
    jsr target_init
    jsr players_s_reset_distance
    ; 'hole_init' routine records width, length and par for current hole.
    jsr hole_s_init

    ; Now jump over the first fourteen bytes (which are the coordinates of the 
    ; target, distance to target, boundaries, par and hazard type).
    lda P0
    clc
    adc #.OFFSET_TO_QUADS
    sta P0
    lda P1
    adc #0
    sta P1
    jsr quads_s_load

    ; Zero-page locations P0-P1 should now point to the byte AFTER the quads
    ; data.  That's the number of waypoints.  Same holds after waypoints have
    ; loaded...
    jsr waypts_load
    jsr trees_load

    ; P0-P1 now holds location of overhead map icon.
    jsr hole_s_load_overhead_map

    ; Finished loading.
    lda R6510
    ora #$02
    sta R6510
    cli

    lda #1
    sta round_v_teeing_off
    ; Set this index to -1, so when ordering players and we're 'teeing off',
    ; will correctly be incremented to 0.
    lda #(-1)
    sta round_current_tee_index
    ; If this is the first (i.e. 0th) hole, playing order doesn't need to
    ; be refreshed.
    lda round_v_current_hole
    beq +
    jsr players_s_refresh_order 
+

    rts
; end sub round_s_load_hole
} ; !zone

; **************************************************

; NOTE: semi-infinite loop, first called by the 'app' module after round has
;       been initialized.  We won't return from this subroutine until the
;       game's over and it's time to show the title screen again.
!zone {
round_loop
    lda round_current_state
    cmp #ROUND_STATE_LOADING_HOLE
    bne +
    jmp .load_hole
+
    cmp #ROUND_STATE_ORDERING_PLAYERS
    bne +
    jmp .order_players
+
    cmp #ROUND_STATE_DRAWING_STATS_DISPLAY
    bne +
    jmp .draw_stats
+
    cmp #ROUND_STATE_ROTATING_WORLD
    bne +
    jmp .rotate_world
+
    cmp #ROUND_STATE_RENDERING_3D_SCENE
    bne +
    jmp .render_3d
+
    cmp #ROUND_STATE_SHOT_IN_PROGRESS
    bne +
    jmp .shot_in_progress
+
    cmp #ROUND_STATE_SHOT_COMPLETE
    bne +
    jmp .shot_complete
+
    cmp #ROUND_STATE_DRAWING_SCORE_CARDS
    bne +
    jmp .score_cards
+
    cmp #round_c_STATE_WIPING_SCREEN
    bne +
    jmp .wiping_screen
+
    jmp round_loop

.load_hole
    jsr round_s_load_hole
    jsr wind_s_init
    jsr slope_s_init
    jsr players_s_prepare_next_hole
    jsr stats_s_clear_balls_holed
    inc round_current_state
    jmp round_loop

.order_players
    jsr round_order_players
    jsr round_set_joystick
    inc round_current_state
    ; FIXME: omit this?!
    jmp round_loop

.draw_stats
    jsr round_swing_or_putt
    inc round_current_state
    jmp round_loop

.rotate_world
    ; Disable all sprites.
    +spr_m_disable_all 
    lda #%10011111
    sta SPMC
    jsr ball_s_hide
    jsr round_rotate_world
    ; NOTE: allow fall through...
.render_3d
    lda round_must_render
    +branch_if_true +
    ; FIXME: can save memory here by calling these 3 routines before checking
    ; value of 'round_must_render'!
    jsr stats_s_draw_name
    jsr stats_s_refresh_hole
    jsr stats_s_draw_current_shots
    jmp .skip_render

+
    sei
    ; FIXME: sometimes screen flashes brown here!  Set 'ground' color to CYAN
    ; before sei?!

    ; FIXME: must also reset bitmap colors (cf. trees!).
    jsr bdrop_s_deactivate_sprites
    +clr SPENA
    jsr gfxs_s_clear_bitmap
    jsr round_s_set_bg_colors
    jsr ingm_s_draw_rough
    jsr stats_s_clear
    jsr stats_s_draw_name
    jsr stats_s_refresh_hole
    jsr stats_s_draw_distance
    jsr stats_s_draw_current_shots
    jsr stats_s_refresh_balls_holed_markers

    jsr gfxs_s_init_colors
    jsr rand_s_reset

    lda #CYAN
    sta BGCOL0
    +utils_m_enable_bitmap_mode 
    +utils_m_enable_multicolor_mode 
    jsr quads_project_and_draw

    jsr bdrop_s_draw_distant_objects
    jsr bdrop_s_draw_bg_trees

    jsr target_s_project
    jsr target_s_draw_shadow
    jsr trees_draw
    jsr target_draw
    jsr winslp_s_draw_box
    jsr round_s_draw_direction
    jsr golfer_s_init_shadow
    
    lda round_v_must_putt
    +branch_if_true +
    jsr hole_s_draw_overhead_map

    lda round_v_teeing_off
    +branch_if_false +
    jsr tmarkers_s_draw
+

    cli

.skip_render
    lda #ROUND_STATE_SHOT_IN_PROGRESS
    sta round_current_state
    jsr target_s_reset
    ; NOTE: still need to redraw name even if not re-rendering 3D!
    ldx joy_v_current_port
    +joy_m_lock_fire
    jsr ball_s_set_at_origin
    jsr golfer_setup_draw

    lda round_v_must_putt
    +branch_if_true +
    jsr hole_s_init_overhead_ball_sprite
+

    sec
    jsr partsys_s_clear_sprite
    ; NOTE: only need this while multiple players are teeing off.  Easiest
    ; just to include it in any case!
    jsr golfer_s_restore_shadow
    jsr powarc_s_reset
    jsr gfxs_s_clear_msg_area
    jsr clubs_s_draw2
    jsr terrain_s_draw_indicator
    jmp round_loop

.shot_in_progress
    ; Nothing to do - routines will be called on 'golfer' and 'ball' modules
    ; via raster interrupts...
    jsr ball_s_update_full_tilt
    jmp round_loop

.shot_complete
    ; FIXME: a bit messy putting these here?!
    ldy round_v_current_player
    jsr stats_s_inc_current_shots

    ; BUG: fix to #24/25.
    ; If ball is in water, force terrain as appropriate (- there's a chance it
    ; might have been changed if we were in middle of full-tilt update when 
    ; ball landed and ball_s_check_collision was called).
    ; NOTE: so we're using 'ball_v_is_in_water' to see if we're in water,
    ; rather than value of 'ball_v_current_terrain'!!!
    lda ball_v_is_in_water
    +branch_if_false .check_bounds
    lda #ball_c_TERRAIN_WATER
    sta ball_v_current_terrain
    bne +

.check_bounds
    lda ball_v_out_of_bounds
    +branch_if_true +

    jsr ball_s_calc_distance_of_shot
    jsr ball_calc_new_world_position
    jsr golfer_store_new_ball_pos
+
    jsr target_set_aa_coords
    jsr target_rotate_to_pos_z_axis
    ; TODO: what if this routine returns code as to whether next routine
    ; should be called?
    jsr golfer_s_record_distance
    bcc +
    jsr ball_s_display_distance_of_shot
+
    inc round_current_state
    jmp round_loop



    
.score_cards
    lda #play_c_MODE_START_SCORE_CARDS
    sta play_v_current_mode 
    rts ; EXIT POINT!

.wiping_screen
    lda transtn_v_is_active
    +branch_if_true +
    lda #ROUND_STATE_LOADING_HOLE
    sta round_current_state
    jsr round_s_prepare_loading_msg
+
    jmp round_loop

; end sub round_loop
} ; !zone

; **************************************************

!zone {
round_handle_shot_complete
    ; Ball has stopped moving.  Current player's position and distance
    ; from target have been updated and recorded.  And they've pressed the
    ; fire button to signal that they're finished contemplating the shot...

    ; TODO: turn off any particle effect - don't want it to continue and
    ; mess up sprite colors.
    jsr partsys_s_deactivate
    jsr golfer_s_hide_shadow

    ; Save the current player's club selection.
    ldx round_v_current_player
    lda clubs_current_selection
    sta players_v_current_club,x

    jsr round_s_check_if_hole_complete
    bcc +

    ; Hole is complete.  Before score cards are displayed, players should be 
    ; ordered as per their round scores.  We can enforce this by setting the
    ; 'round_v_teeing_off' flag before routine call.
    lda #ROUND_STATE_DRAWING_SCORE_CARDS
    sta round_current_state
    +set round_v_teeing_off
    jsr players_s_refresh_order
    rts ; EXIT POINT.

+
    ; If ball landed in water, this player should retake their shot
    ; immediately.
    lda ball_v_out_of_bounds
    +branch_if_true ++
    lda ball_v_is_in_water
    +branch_if_false +
++
    +clr round_must_render
    lda #ROUND_STATE_DRAWING_STATS_DISPLAY   
    jmp ++
+
    lda #ROUND_STATE_ORDERING_PLAYERS
++
    sta round_current_state
    rts
; end sub round_handle_shot_complete
} ; !zone

; **************************************************

; The object of the routine is to get the correct index into
; round_v_current_player.  I.e. who's the next player up?
!zone {
round_order_players
    lda #1
    sta round_must_render

    lda round_v_teeing_off
    +branch_if_false .mid_hole
    inc round_current_tee_index
    lda round_current_tee_index
    cmp shared_v_num_players
    bne .still_teeing_off

    +clr round_v_teeing_off
    jmp .mid_hole

.still_teeing_off
    ; NOTE: round_current_tee_index is still in .A.
    tax
    beq +
    dec round_must_render
+   
    lda players_v_playing_order,x
    sta round_v_current_player
    rts ; EXIT POINT.

.mid_hole
    jsr players_s_refresh_order
    lda players_v_playing_order
    sta round_v_current_player
    rts
; end sub round_order_players
} ; !zone

; **************************************************

!zone {
round_rotate_world
    ; Set ball's world position for current player.
    ldx round_v_current_player
    lda players_v_ball_pos_x_lo,x
    sta ball_world_x_lo
    lda players_v_ball_pos_x_hi,x
    sta ball_world_x_hi
    lda players_v_ball_pos_z_lo,x
    sta ball_world_z_lo
    lda players_v_ball_pos_z_hi,x
    sta ball_world_z_hi

    ; NOTE: force a world position with following code - useful for debugging.
;    lda #$e2
;    sta ball_world_x_lo
;    lda #$ff
;    sta ball_world_x_hi
;    lda #$93
;    sta ball_world_z_lo
;    lda #$12
;    sta ball_world_z_hi

    ; Move quads, target & trees relative to ball's (world) position.
    jsr quads_set_aa_coords
    jsr target_set_aa_coords
    jsr trees_set_aa_coords
    jsr hole_s_set_aa_boundaries

    ; Work out rotation based on current waypoint.
    jsr waypts_find_index
    jsr waypts_calc_rotation
    ; NOTE: we still want to rotate the world even if waypoint dictates there
    ; should be no rotation - otherwise the wind indicator doesn't get 
    ; initialized!

    ; Rotate world accordingly...
    jsr quads_rotate
    jsr target_rotate
    jsr trees_rotate
    jsr round_windslope_rotate
    jsr golfer_s_rotate_direction

    +target_reset_hw_sprite

    rts
; end sub round_rotate_world
} ; !zone

; **************************************************

; Determine whether the current player should prepare a swing (with wood,
; iron or wedge) or a putt.  Depends on both distance from target and current
; terrain.
!zone {
round_swing_or_putt
    +set round_can_change_club

    ldx round_v_current_player

    ; First check the terrain - anything other than green/fairway and it's
    ; a swing.
    lda players_v_terrain,x
    ; Put this in MATHS0 - we might use it again later.
    sta MATHS0
    cmp #ball_c_TERRAIN_GREEN_FWAY
    bne .swing

    ; Terrain OK so check distance...
    lda players_v_distance_lo,x
    sec
    sbc #round_c_MAX_PUTT_DISTANCE_LO
    lda players_v_distance_hi,x
    sbc #round_c_MAX_PUTT_DISTANCE_HI
    bcs .swing

    ; Putt.
    ; Force golf club selection - when putter is selected, this isn't recorded
    ; in 'players' module.
    lda #CLUBS_PUTTER_I
    sta clubs_current_selection
    lda #1
    sta round_v_must_putt
    ; FIXME: better off in a separate routine (also below).
    ; Set correct X-position for golfer.
    lda #GOLFER_POS_X_LO+GOLFER_POS_PUTTING_X_OFFSET 
    sta spr_v_x_lo
    sta spr_v_x_lo+1
    rts ; EXIT POINT.

.swing
    ; If player's in a bunker, pre-select SW (and don't allow them to change
    ; it!).  NOTE: we previously stored terrain type in MATHS0.
    lda MATHS0
    cmp #ball_c_TERRAIN_BUNKER
    bne +
    dec round_can_change_club
    lda #clubs_c_SW
    jmp .set_club   
+
    lda players_v_current_club,x
    ; Quick check to see if we've got the putter.  If so, switch to PW.
    ; TODO: could check terrain and pre-select SW if appropriate...(?!)
    cmp #CLUBS_PUTTER_I
    bne .check_for_driver
    lda #clubs_c_PW 
    bne .set_club
.check_for_driver
    ; If driver but not on tee, reduce to 3-wood.
    ldy round_v_teeing_off
    bne .set_club   ; We are teeing off, so anything goes.
    cmp #clubs_c_DR 
    bne .set_club
    lda #clubs_c_3W
.set_club
    sta clubs_current_selection
    +clr round_v_must_putt
    rts
; end sub round_swing_or_putt
} ; !zone

; **************************************************

; OUTPUT:   C flag clear if hole still being played; C flag set if it's
;           finished.
!zone {
round_s_check_if_hole_complete
    ldx #0
-   lda players_v_current_shots,x
    bpl .ok
    inx
    cpx shared_v_num_players
    bne -
    ; The hole is finished - negative bit is set for all players'
    ; 'current_shots' values.

    jsr terrain_s_clear_indicator
    jsr sc_s_calc_team_scores

    inc round_v_current_hole
    sec
    rts ; EXIT POINT.

.ok
    clc
    rts
; end sub round_s_check_if_hole_complete
} ; !zone

; **************************************************

!zone {
round_windslope_rotate
    lda round_v_must_putt
    +branch_if_true .putt

    ; It's a swing, so rotate the wind.
    jsr wind_s_rotate
    rts ; EXIT POINT.

.putt
    jsr slope_s_rotate
    rts
; end sub round_windslope_rotate
} ; !zone

; **************************************************

!zone {
round_s_draw_direction
    jsr winslp_s_draw_direction

    lda round_v_must_putt
    +branch_if_true .putt

    jsr wind_s_draw_speed
    rts ; EXIT POINT.

.putt
    jsr slope_s_draw_tiles

    rts
; end sub round_s_draw_direction
} ; !zone

; **************************************************

!zone {
round_set_joystick
    ldx round_v_current_player
    lda shared_v_player_joysticks,x
    sta joy_v_current_port
    rts
; end sub round_set_joystick
} ; !zone

; **************************************************

!zone {
round_s_set_bg_colors
    lda #CYAN
    sta round_current_sky_color
    lda #ORANGE
    sta round_v_current_ground_color
    rts
; end sub round_s_set_bg_colors
} ; !zone

; **************************************************

!zone {
round_s_prepare_loading_msg
    jsr msg_s_clear

    lda #font_c_ASCII_SPACE 
    sta round_c_LOADING_MSG+14

    ldx round_v_current_hole
    inx
    stx P0
    lda #0
    sta P1
    jsr utils_s_16bit_hex_to_dec
    lda #<round_c_LOADING_MSG+13
    sta P0
    lda #>round_c_LOADING_MSG+13
    sta P1
    jsr utils_s_write_digits_to_buffer

    lda #<round_c_LOADING_MSG
    sta P0
    lda #>round_c_LOADING_MSG
    sta P1
    lda #round_c_LOADING_MSG_LEN 
    sta P4
    jsr msg_s_display

    rts
; end sub round_s_prepare_loading_msg
} ; !zone

; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************

round_c_SIZE = *-round_c_BEGIN

