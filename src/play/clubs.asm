; Top-hole Golf
; Copyright 2020 Matthew Clarke


clubs_c_BEGIN = *


; *****************
; *** CONSTANTS ***
; *****************
; NOTE: this excludes the putter:
CLUBS_N = 12
CLUBS_PUTTER_I = CLUBS_N
; Power levels for each club.
clubs_l_VZ_LO   !byte <8686, <7887 ,<7079, <5925, <5553, <4922, <4572, <4096, <3904
                !byte <3629, <2933, <2271, <1504
clubs_l_VZ_HI   !byte >8686, >7887 ,>7079, >5925, >5553, >4922, >4572, >4096, >3904
                !byte >3629, >2933, >2271, >1504
clubs_l_VY_LO   !byte <1375, <1391 ,<1376, <1477, <1487, <1411, <1398, <1331, <1343
                !byte <1307, <1368, <1419, 0
clubs_l_VY_HI   !byte >1375, >1391 ,>1376, >1477, >1487, >1411, >1398, >1331, >1343
                !byte >1307, >1368, >1419, 0

CLUBS_STR !scr "DR3W5W3I4I5I6I7I8I9IPWSWPT"
clubs_l_DISTANCE_STR !scr "240230215205195175165150145135120100"   
clubs_l_DISTANCE_STR_OFFSETS !byte 0,3,6,9,12,15,18,21,24,27,30,33
; NOTE: first three chars are placeholders for actual distance.
clubs_l_DISTANCE_BUFF !raw "000yds"
clubs_l_DISTANCE_PUTT_STR !raw "70ft  "
clubs_c_BUFF_LEN = 6

clubs_c_DR = 0
clubs_c_3W = 1
clubs_c_PW = 10
clubs_c_SW = 11

; NOTE: remember there's also an offscreen copy of the power-arc!!!
CLUBS_STR_DEST = gfxs_c_BITMAP_BASE+(18*40*8)+(4*8)
CLUBS_STR_DEST2 = powarc_v_bitmap_copy+(4*8)  

CLUBS_CHARS
    !bin "../../assets/chars/club_font.bin"
CLUBS_STR2  
    !byte   12,13   ; DR
    !byte   0,7     ; 3W
    !byte   2,7     ; 5W
    !byte   0,8     ; 3I
    !byte   1,8
    !byte   2,8
    !byte   3,8
    !byte   4,8
    !byte   5,8
    !byte   6,8
    !byte   9,7     ; PW
    !byte   10,7    ; SW
    !byte   9,11    ; PT


; *****************
; *** VARIABLES ***
; *****************
clubs_current_selection !byte   0


; *******************
; ****** MACROS *****
; *******************


; *******************
; *** SUBROUTINES ***
; *******************
!zone {
clubs_reset
    lda #0
    sta clubs_current_selection
    rts
; end sub clubs_reset
} ; !zone

; **************************************************

; OUTPUTS:  C clear if allowed; C set if not.
; NOTE: implement wraparound.
!zone {
clubs_s_next
    lda round_can_change_club
    +branch_if_true +
    sec
    rts ; EXIT POINT - not allowed to change club.

+
    ldx clubs_current_selection
    inx
    cpx #CLUBS_N
    bne .set_club

    ; Driver not allowed unless we're teeing off.
    ; So if 'round_v_teeing_off' is true (=1), club = 0 (driver);
    ; else 1 (3-wood).
    lda round_v_teeing_off
    eor #$01
    tax

.set_club
    stx clubs_current_selection

    ldy #sfx_c_BROWSE
    jsr snd_s_init_sfx

    clc
    rts
; end sub clubs_s_next
} ; !zone

; **************************************************

; OUTPUTS:  C clear if allowed; C set if not.
; NOTE: implement wraparound.
!zone {
clubs_s_prev
    lda round_can_change_club
    +branch_if_true +
    sec
    rts ; EXIT POINT - not allowed to change club.

+
    ldx clubs_current_selection
    dex
    bne +
    ; Driver selected.  Disallow this unless teeing off.
    lda round_v_teeing_off
    +branch_if_true .set_club
    jmp ++
+
    bpl .set_club
++
    ldx #CLUBS_N-1
.set_club
    stx clubs_current_selection

    ldy #sfx_c_BROWSE
    jsr snd_s_init_sfx

    clc
    rts
; end sub clubs_s_prev
} ; !zone

; **************************************************

!zone {
clubs_s_draw2
    ; Address of text into P0-P1.
    lda clubs_current_selection
    asl
    clc
    adc #<CLUBS_STR
    sta P0
    lda #>CLUBS_STR
    adc #0
    sta P1
    lda #2
    sta P4
    lda #19*8
    sta P2
    lda #7*4
    sta P3
    jsr font_s_draw_text
    jsr clubs_s_draw_distance

    rts
; end sub clubs_s_draw2
} ; !zone

; **************************************************

; Draw average maximum distance for this club in the 'messaging' area.
!zone {
clubs_s_draw_distance
    ldx clubs_current_selection
    cpx #CLUBS_PUTTER_I 
    bne +
    ; Fixed string for the putter.
    lda #<clubs_l_DISTANCE_PUTT_STR 
    sta P0
    lda #>clubs_l_DISTANCE_PUTT_STR 
    sta P1
    ; NOTE: high byte of this address can't be zero!
    bne ++

+
    lda clubs_l_DISTANCE_STR_OFFSETS,x
    tax
    ; Now we have offset for the distance string. Copy 3 bytes to the start
    ; of the buffer (before 'YDS').
    ldy #0
-
    lda clubs_l_DISTANCE_STR,x
    sta clubs_l_DISTANCE_BUFF,y
    inx
    iny
    cpy #3
    bne -

    ; Prepare call to msg_s_display.
    lda #<clubs_l_DISTANCE_BUFF
    sta P0
    lda #>clubs_l_DISTANCE_BUFF
    sta P1
++
    lda #clubs_c_BUFF_LEN
    sta P4

    jsr msg_s_display

    rts
; end sub clubs_s_draw_distance
} ; !zone

; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************

clubs_c_SIZE = *-clubs_c_BEGIN

