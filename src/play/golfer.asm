; Top-hole Golf
; Copyright 2020 Matthew Clarke


golfer_c_BEGIN = *


; *****************
; *** CONSTANTS ***
; *****************
GOLFER_CLUB_SHAFT_SPR_NUM   = 1
GOLFER_UPPER_BODY_SPR_NUM   = 2
GOLFER_LOWER_BODY_SPR_NUM   = 3

GOLFER_POWERING_UP = 20
GOLFER_SETTING_SPIN = 21

GOLFER_POS_X_LO = spr_c_VISIBLE_ALL_L+116
GOLFER_POS_X_HI = 0
GOLFER_POS_Y_UPPER = spr_c_VISIBLE_ALL_T+140
GOLFER_POS_Y_LOWER = spr_c_VISIBLE_ALL_T+161
GOLFER_SHADOW_POS_Y = spr_c_VISIBLE_ALL_T+182
GOLFER_SHADOW_POS_X_LO = spr_c_VISIBLE_ALL_L+110
; Add this to golfer and shadow sprites when putting.
GOLFER_POS_PUTTING_X_OFFSET = 24

GOLFER_HOLD_TIME = 6
golfer_c_HOLD_TIME_SWING_COMPLETE = 40

; When animation sequence gets to this frame, initiate ball flight.
GOLFER_STRIKE_FRAME = 174

GOLFER_POS_MARKER_SPR_NUM = 6
GOLFER_POS_MARKER_SPR_PTR = 162
GOLFER_POS_MARKER_Y = spr_c_VISIBLE_ALL_T+150-2;-24
GOLFER_POS_MARKER_X = spr_c_VISIBLE_ALL_L+160-2
GOLFER_POS_MARKER_MIN_X = GOLFER_POS_MARKER_X-50
GOLFER_POS_MARKER_MAX_X = GOLFER_POS_MARKER_X+50
GOLFER_POS_MARKER_VX_LO = 200
GOLFER_POS_MARKER_SLOW_VX_LO = 32
GOLFER_POS_MARKER_VX_HI = 0
; NOTE: 0th value is a dummy to make sure everything's in the right place.  If
; offset is 0, no adjustment to the velocity vectors is necessary...
GOLFER_POS_MARKER_TRIG_INDICES !byte 0,3,7,10,13,17,20,23,26,29,32,35,38,42,45,48,51,53,56,59,62,65,68,70,73,76,78,81,83,85,88,90,93,95,97,99,101,104,106,108,110,112,114,116,117,119,121,123,125,126,128

!zone {
.BX = spr_c_VISIBLE_ALL_L-2 ; minus 2 because sprite is 5 pixels wide. 
.BY = spr_c_VISIBLE_ALL_T 
golfer_l_MARKER_X_POS !byte .BX+125,.BX+131,.BX+137,.BX+143,.BX+148,.BX+154,.BX+160,.BX+166,.BX+172,.BX+177,.BX+183,.BX+189,.BX+195
golfer_l_MARKER_Y_POS !byte .BY+150,.BY+149,.BY+149,.BY+148,.BY+148,.BY+147,.BY+147,.BY+147,.BY+148,.BY+148,.BY+149,.BY+149,.BY+150
golfer_l_MARKER_X_POS2
    !byte   .BX+130,.BX+132,.BX+134,.BX+136,.BX+138,.BX+140,.BX+142,.BX+144,.BX+146,.BX+148,.BX+149,.BX+151,.BX+153,.BX+155,.BX+157,.BX+159,.BX+160
    !byte   .BX+161,.BX+163,.BX+165,.BX+167,.BX+169,.BX+171,.BX+172,.BX+174,.BX+176,.BX+178,.BX+180,.BX+182,.BX+184,.BX+186,.BX+188,.BX+190
golfer_l_MARKER_Y_POS2
    !byte   .BY+150,.BY+149,.BY+149,.BY+149,.BY+149,.BY+148,.BY+148,.BY+148,.BY+148,.BY+148,.BY+148,.BY+148,.BY+147,.BY+147,.BY+147,.BY+147,.BY+147
    !byte   .BY+147,.BY+147,.BY+147,.BY+147,.BY+148,.BY+148,.BY+148,.BY+148,.BY+148,.BY+148,.BY+148,.BY+149,.BY+149,.BY+149,.BY+149,.BY+150
} ; !zone
golfer_l_TRIG_INDICES !byte 6,5,4,3,2,1,0,63,62,61,60,59,58
golfer_c_MARKER_MOVE_DELAY = 2
; NOTE: index this table with direction (spr_c_LEFT/RIGHT).
golfer_l_MARKER_BOUNDS !byte 0,32
golfer_l_MARKER_DELTA !byte (-1),1

GOLFER_BACKSWING_FRAME_RATE = 7
GOLFER_FORESWING_FRAME_RATE = 7
GOLFER_PUTTING_FRAME_RATE = 8 

; NOTE: INTEGER division!
GOLFER_PUTT_POWER_INCREMENT = 256 / (sprd_c_GOLFER_PUTT_BACKSWING_FRAMES*GOLFER_PUTTING_FRAME_RATE)

; Number of frames either side of 'ball-contact' frame that determine ball
; spin.  If button was pressed beyond or at this threshold, that's max spin
; (hook or slice). 
GOLFER_SPIN_THRESHOLD = 6
; (For full-swing) there are 28 frames from top-of-swing to ball contact.
GOLFER_FRAMES_TO_CONTACT = 28
GOLFER_PRECISION_ZONE_START = GOLFER_FRAMES_TO_CONTACT - GOLFER_SPIN_THRESHOLD
GOLFER_PRECISION_ZONE_END   = GOLFER_FRAMES_TO_CONTACT + GOLFER_SPIN_THRESHOLD
GOLFER_CLUB_NORMAL_COLOR = BLACK
GOLFER_CLUB_HIGHLIGHT_COLOR = GREY1

GOLFER_CROSSHAIR_SPR_NUM = 6

; Where to position sprites in swing animation sequence.
GOLFER_BASE_X = spr_c_VISIBLE_ALL_L+128
GOLFER_BASE_Y = spr_c_VISIBLE_ALL_T+140

GOLFER_UPPER_BODY_POS_X
    !byte   GOLFER_BASE_X
    !byte   GOLFER_BASE_X

    !byte   GOLFER_BASE_X

    !byte   GOLFER_BASE_X
    !byte   GOLFER_BASE_X
    !byte   GOLFER_BASE_X
    !byte   GOLFER_BASE_X
    !byte   GOLFER_BASE_X+2
    !byte   GOLFER_BASE_X+2
    !byte   GOLFER_BASE_X+2
    !byte   GOLFER_BASE_X+2
    !byte   GOLFER_BASE_X
    !byte   GOLFER_BASE_X-2
    !byte   GOLFER_BASE_X-3
GOLFER_UPPER_BODY_LOOKING_UP_POS_Y = GOLFER_BASE_Y+5
GOLFER_UPPER_BODY_POS_Y
    !byte   GOLFER_BASE_Y+6
    !byte   GOLFER_BASE_Y+6

    !byte   GOLFER_BASE_Y+6

    !byte   GOLFER_BASE_Y+6
    !byte   GOLFER_BASE_Y+6
    !byte   GOLFER_BASE_Y+3
    !byte   GOLFER_BASE_Y+9
    !byte   GOLFER_BASE_Y+7
    !byte   GOLFER_BASE_Y+5
    !byte   GOLFER_BASE_Y+6
    !byte   GOLFER_BASE_Y+8
    !byte   GOLFER_BASE_Y+7
    !byte   GOLFER_BASE_Y
    !byte   GOLFER_BASE_Y
GOLFER_LOWER_BODY_POS_X
    !byte   GOLFER_BASE_X
    !byte   GOLFER_BASE_X

    !byte   GOLFER_BASE_X

    !byte   GOLFER_BASE_X
    !byte   GOLFER_BASE_X
    !byte   GOLFER_BASE_X
    !byte   GOLFER_BASE_X
    !byte   GOLFER_BASE_X+2
    !byte   GOLFER_BASE_X+2
    !byte   GOLFER_BASE_X+2
    !byte   GOLFER_BASE_X+2
    !byte   GOLFER_BASE_X
    !byte   GOLFER_BASE_X-2
    !byte   GOLFER_BASE_X-3
; NOTE: lower body y is always GOLFER_BASE_Y+21.
GOLFER_CLUB_X
    !byte   GOLFER_BASE_X+12
    !byte   GOLFER_BASE_X+14

    !byte   GOLFER_BASE_X-2

    !byte   GOLFER_BASE_X-12
    !byte   GOLFER_BASE_X-18
    !byte   GOLFER_BASE_X-2
    !byte   GOLFER_BASE_X-12
    !byte   GOLFER_BASE_X-16
    !byte   GOLFER_BASE_X+0
    !byte   GOLFER_BASE_X+18
    !byte   GOLFER_BASE_X+22
    !byte   GOLFER_BASE_X-10
    !byte   GOLFER_BASE_X-6
    !byte   GOLFER_BASE_X
GOLFER_CLUB_Y
    !byte   GOLFER_BASE_Y+23
    !byte   GOLFER_BASE_Y+23

    !byte   GOLFER_BASE_Y+21

    !byte   GOLFER_BASE_Y+7
    !byte   GOLFER_BASE_Y-7
    !byte   GOLFER_BASE_Y-6
    !byte   GOLFER_BASE_Y+3
    !byte   GOLFER_BASE_Y+6
    !byte   GOLFER_BASE_Y+21
    !byte   GOLFER_BASE_Y+24
    !byte   GOLFER_BASE_Y+21
    !byte   GOLFER_BASE_Y-7
    !byte   GOLFER_BASE_Y-8
    !byte   GOLFER_BASE_Y-2

GOLFER_PUTT_POS_X = GOLFER_BASE_X+12
GOLFER_PUTT_POS_Y = GOLFER_BASE_Y
; FIXME: NOTE: always the same offsets!!!
GOLFER_PUTT_CLUB_SHAFT_OFFSETS_X = GOLFER_PUTT_POS_X+10
GOLFER_PUTT_CLUB_SHAFT_OFFSETS_Y = GOLFER_BASE_Y+21+3

GOLFER_WATCHING_UPPER_OFFSET_X = GOLFER_BASE_X-4
GOLFER_WATCHING_UPPER_OFFSET_Y = GOLFER_BASE_Y+1
GOLFER_WATCHING_CLUB_OFFSET_X = GOLFER_BASE_X-2
GOLFER_WATCHING_CLUB_OFFSET_Y = GOLFER_BASE_Y-20

golfer_c_STATE_READY                    = 0
golfer_c_STATE_LOOKING_UP               = 1
golfer_c_STATE_POWERING_UP              = 2
golfer_c_STATE_HOLD                     = 3
golfer_c_STATE_PRECISION                = 4
golfer_c_STATE_HOLD2                    = 5
golfer_c_STATE_ANIMATE_BACKSWING        = 6
golfer_c_STATE_ANIMATE_HOLD             = 7
golfer_c_STATE_ANIMATE_FORESWING        = 8
golfer_c_STATE_ANIMATE_HOLD2            = 9
golfer_c_STATE_FINISHED                 = 10
golfer_c_STATE_ANIMATE_BACKSWING_PUTT   = 11
golfer_c_STATE_ANIMATE_HOLD_PUTT        = 12
golfer_c_STATE_ANIMATE_FORESWING_PUTT   = 13
golfer_c_STATE_IN_LIMBO                 = 14

golfer_c_HOLD_DURATION = 20
golfer_c_HOLD_DURATION_SWING = 10

golfer_c_BASE_DIRECTION_X_LO = 0
golfer_c_BASE_DIRECTION_X_HI = 0
golfer_c_BASE_DIRECTION_Z_LO = 255
golfer_c_BASE_DIRECTION_Z_HI = 0

golfer_c_MARKER_INITIAL_INDEX = 16

golfer_l_SHADOW_PATTERN !byte $ff,$ff,$ff,$f3,$fc,$f3,$fc,$f3
                        !byte $ff,$ff,$ff,$33,$cf,$33,$cc,$33
golfer_l_SHADOW_POS_LO  !byte <gfxs_c_BITMAP_BASE+(22*40*8+15*8),<gfxs_c_BITMAP_BASE+(22*40*8+16*8)
golfer_l_SHADOW_POS_HI  !byte >gfxs_c_BITMAP_BASE+(22*40*8+15*8),>gfxs_c_BITMAP_BASE+(22*40*8+16*8)
golfer_l_ANTI_SHADOW_BUFFER !fill   16
golfer_l_SHADOW_BUFFER      !fill   16

golfer_c_MIN_PRAISEWORTHY_PUTT_FT = 15*hole_c_PIXELS_PER_FOOT 

golfer_c_RESPONSE_NO    = 0
golfer_c_RESPONSE_YES   = 1
golfer_l_RESPONSE_OFFSET_BEGIN  !byte   14,17
; NOTE: actually one-past-end!
golfer_l_RESPONSE_OFFSET_END    !byte   16,20
golfer_c_RESPONSE_GHOST_COLOR       = GREY1
golfer_c_RESPONSE_HIGHLIGHT_COLOR   = YELLOW


; *****************
; *** VARIABLES ***
; *****************
golfer_v_current_state        !byte   0
golfer_look_up_count        !byte   0
golfer_v_frame_count          !byte   0
golfer_has_hit_ball         !byte   0
; NOTE: negative value indicates that it isn't moving.
golfer_pos_marker_direction !byte   $ff
golfer_v_shot_power           !byte   0
golfer_shot_power_finalized !byte   0
golfer_v_shot_spin_set        !byte   0
golfer_v_crosshair_active     !byte   0
; Keep track of this so we can re-position sprites during animation.
golfer_anim_frame   !byte   0
; FIXME: HACK!!!
golfer_keyboard_locked  !byte   0

; NOTE: a unit vector.
golfer_v_direction_x_lo !byte   0
golfer_v_direction_x_hi !byte   0
golfer_v_direction_z_lo !byte   0
golfer_v_direction_z_hi !byte   0

golfer_v_marker_index       !byte   0
golfer_v_marker_move_count  !byte   0

golfer_v_is_hidden  !byte   0
; When golfer hidden, record previous pointers here so they can later be
; restored.
; FIXME: necessary?  Not always the same?!
golfer_v_spr_pointer_buffer !fill   3

golfer_v_current_skin_tone  !byte   0
; For concede requests.
golfer_v_current_response       !byte   0
golfer_v_concede_query_active   !byte   0


; *******************
; ****** MACROS *****
; *******************
!macro golfer_set_club_color .color {
    lda #.color
    sta spr_v_color+GOLFER_CLUB_SHAFT_SPR_NUM
} ; golfer_set_club_color

!macro golfer_m_init_hold .frames,.new_state {
    lda #.frames
    sta golfer_v_frame_count
    lda #.new_state
    sta golfer_v_current_state
} ; golfer_m_init_hold

!macro golfer_m_update_hold {
    dec golfer_v_frame_count
} ; golfer_m_update_hold


; *******************
; *** SUBROUTINES ***
; *******************
!zone {
golfer_draw
    ldx #GOLFER_CLUB_SHAFT_SPR_NUM
    ldy #GOLFER_CLUB_SHAFT_SPR_NUM
    jsr spr_s_write_to_vic_ii

    ldx #GOLFER_UPPER_BODY_SPR_NUM
    ldy #GOLFER_UPPER_BODY_SPR_NUM
    jsr spr_s_write_to_vic_ii

    ldx #GOLFER_LOWER_BODY_SPR_NUM
    ldy #GOLFER_LOWER_BODY_SPR_NUM
    jsr spr_s_write_to_vic_ii

    lda golfer_v_crosshair_active
    +branch_if_false +
    ldx #GOLFER_CROSSHAIR_SPR_NUM
    ldy #GOLFER_CROSSHAIR_SPR_NUM
    jsr spr_s_write_to_vic_ii

+
    ldx #partsys_c_SPR_NUM
    ldy #partsys_c_SPR_NUM
    jsr spr_s_write_to_vic_ii

    rts
; end sub golfer_draw
} ; !zone

; **************************************************

!zone {
golfer_s_init_swing_animation
    ; Torso.
    lda #sprd_c_GOLFER_SWING_UPPER 
    sta spr_v_anim_start_ptr+GOLFER_UPPER_BODY_SPR_NUM
    ; Make sure we're not looking up!
    sta spr_v_current_ptr+GOLFER_UPPER_BODY_SPR_NUM

    lda GOLFER_UPPER_BODY_POS_Y
    sta spr_v_y+GOLFER_UPPER_BODY_SPR_NUM

    lda #sprd_c_GOLFER_SWING_UPPER+sprd_c_GOLFER_BACKSWING_FRAMES-1  
    sta spr_v_anim_end_ptr+GOLFER_UPPER_BODY_SPR_NUM
    ; Legs.
    lda #sprd_c_GOLFER_SWING_LOWER 
    sta spr_v_current_ptr+GOLFER_LOWER_BODY_SPR_NUM
    lda #sprd_c_GOLFER_SWING_LOWER+sprd_c_GOLFER_BACKSWING_FRAMES-1
    sta spr_v_anim_end_ptr+GOLFER_LOWER_BODY_SPR_NUM
    lda #sprd_c_GOLFER_SWING_CLUB
    sta spr_v_current_ptr+GOLFER_CLUB_SHAFT_SPR_NUM
    lda #sprd_c_GOLFER_SWING_CLUB+sprd_c_GOLFER_BACKSWING_FRAMES-1
    sta spr_v_anim_end_ptr+GOLFER_CLUB_SHAFT_SPR_NUM

    lda #1
    sta spr_v_anim_seq_inc+GOLFER_UPPER_BODY_SPR_NUM
    sta spr_v_anim_seq_inc+GOLFER_LOWER_BODY_SPR_NUM
    sta spr_v_anim_seq_inc+GOLFER_CLUB_SHAFT_SPR_NUM

    lda #GOLFER_BACKSWING_FRAME_RATE
    sta spr_v_anim_timer+GOLFER_UPPER_BODY_SPR_NUM
    sta spr_v_anim_timer+GOLFER_LOWER_BODY_SPR_NUM
    sta spr_v_anim_timer+GOLFER_CLUB_SHAFT_SPR_NUM
    sta spr_v_framerate+GOLFER_UPPER_BODY_SPR_NUM
    sta spr_v_framerate+GOLFER_LOWER_BODY_SPR_NUM
    sta spr_v_framerate+GOLFER_CLUB_SHAFT_SPR_NUM

    rts
; end sub golfer_s_init_swing_animation
} ; !zone

; **************************************************

; NOTE: these three routines called by raster interrupt callback.
; This update routine is for the animations.
!zone {
golfer_s_update_anim
    lda golfer_v_current_state

    cmp #golfer_c_STATE_READY
    bne +
    jmp .looking_down
+
    cmp #golfer_c_STATE_LOOKING_UP
    bne +
    jmp .looking_up
+
    cmp #golfer_c_STATE_POWERING_UP
    bne +
    jmp .powering_up
+
    cmp #golfer_c_STATE_HOLD
    bne +
    jmp .hold
+
    cmp #golfer_c_STATE_PRECISION
    bne +
    jmp .precision
+
    cmp #golfer_c_STATE_HOLD2
    bne +
    jmp .hold2
+
    cmp #golfer_c_STATE_ANIMATE_BACKSWING
    bne +
    jmp .backswing
+
    cmp #golfer_c_STATE_ANIMATE_HOLD
    bne +
    jmp .hold_animate
+
    cmp #golfer_c_STATE_ANIMATE_FORESWING
    bne +
    jmp .foreswing
+
    cmp #golfer_c_STATE_ANIMATE_HOLD2
    bne +
    jmp .animate_hold2
+
    cmp #golfer_c_STATE_ANIMATE_BACKSWING_PUTT
    bne +
    jmp .backswing_putt
+
    cmp #golfer_c_STATE_ANIMATE_HOLD_PUTT
    bne +
    jmp .hold_putt
+
    cmp #golfer_c_STATE_ANIMATE_FORESWING_PUTT
    bne +
    jmp .foreswing_putt
    rts ; EXIT POINT.

.looking_down
    jsr golfer_s_update_looking_down
    jsr golfer_s_move_marker
    rts ; EXIT POINT.
.looking_up
    jsr golfer_s_update_looking_up
    jsr golfer_s_move_marker
    rts ; EXIT POINT.
.powering_up
.precision
    jsr powarc_s_update
    rts ; EXIT POINT.
.hold
    jsr golfer_s_update0_hold
    rts ; EXIT POINT.
.hold2
    jsr golfer_s_update0_hold2
    rts ; EXIT POINT.
.backswing
    jsr golfer_s_animate_swing
    bcc +
    jsr golfer_s_init_animate_hold
+
    rts ; EXIT POINT.
.hold_animate
    jsr golfer_s_update0_hold_animate   
    rts ; EXIT POINT.
.foreswing
    jsr golfer_s_animate_swing
    bcc +
    +golfer_m_init_hold golfer_c_HOLD_TIME_SWING_COMPLETE,golfer_c_STATE_ANIMATE_HOLD2
+
    rts ; EXIT POINT.
.animate_hold2
    jsr golfer_s_update0_hold2_animate
    rts ; EXIT POINT.
.backswing_putt
    ldx #1
    jsr golfer_advance_putting_animation
    bcc +
    +golfer_m_init_hold golfer_c_HOLD_DURATION,golfer_c_STATE_ANIMATE_HOLD_PUTT
+
    rts ; EXIT POINT.
.hold_putt
    jsr golfer_s_update0_hold_putt
    rts ; EXIT POINT.
.foreswing_putt
    ldx #1
    jsr golfer_advance_putting_animation
    bcc +
    lda #golfer_c_STATE_FINISHED
    sta golfer_v_current_state
+
    rts

} ; !zone

; **************************************************

; Check the controls here.
; TODO: joystick #1 or #2?!
; FIXME: TIDY THIS MESS UP!!!
!zone {
golfer_s_update1
    lda golfer_v_current_state
    cmp #golfer_c_STATE_READY
    beq .preswing
    cmp #golfer_c_STATE_LOOKING_UP
    beq .preswing
    cmp #golfer_c_STATE_POWERING_UP
    beq .powering_up
    cmp #golfer_c_STATE_PRECISION
    beq .precision
    cmp #golfer_c_STATE_FINISHED
    beq .finished
    rts

.preswing
    jsr golfer_s_update1_preswing
    rts ; EXIT POINT.

.powering_up
    jsr golfer_s_update1_powering_up
    rts ; EXIT POINT.

.precision
    jsr golfer_s_update1_precision
    rts ; EXIT POINT.

.finished
    jsr golfer_s_update1_finished
    rts

} ; !zone

; **************************************************

!zone {
golfer_update2
    jsr golfer_draw
    rts
; end sub golfer_update2
} ; !zone

; **************************************************

!zone {
golfer_setup_draw
    ; Load in the correct sprite sequence (for animation).
    ldx round_v_current_player
    lda shared_v_player_genders,x
    asl
    tax
    lda round_v_must_putt
    +branch_if_false +
    inx
+
    jsr sstore_s_load_sequence


    ; TODO: custom colours?
    ; Set skin & hair color.
    ldx round_v_current_player
    lda shared_v_player_shirt_color_indices,x
    tay
    lda shared_l_PLAYER_SHIRT_COLORS,y
    pha
    lda shared_v_player_skin_tones,x
    tay
    lda shared_l_PLAYER_SKIN_TONES,y
    sta SPMC0
    sta golfer_v_current_skin_tone
    lda shared_l_PLAYER_HAIR_COLORS,y
    sta SPMC1

    pla
    sta spr_v_color+GOLFER_UPPER_BODY_SPR_NUM   
    lda #GREY1
    sta spr_v_color+GOLFER_LOWER_BODY_SPR_NUM
    lda #BLACK
    sta spr_v_color+GOLFER_CLUB_SHAFT_SPR_NUM
    ; Enable sprites #1-3.
    lda SPENA
    ora #%00001110
    sta SPENA
    lda #0
    sta XXPAND

    lda #0
    sta golfer_has_hit_ball
    sta golfer_v_shot_spin_set
    sta golfer_anim_frame
    sta golfer_v_is_hidden
    sta golfer_v_concede_query_active

    ; Crosshair.
    jsr golfer_reset_crosshair

    lda round_v_must_putt
    +branch_if_true .putt

    ; TODO: male or female?
    lda #sprd_c_GOLFER_SWING_UPPER 
    sta spr_v_current_ptr+GOLFER_UPPER_BODY_SPR_NUM   
    lda #sprd_c_GOLFER_SWING_LOWER 
    sta spr_v_current_ptr+GOLFER_LOWER_BODY_SPR_NUM   
    lda #sprd_c_GOLFER_SWING_CLUB
    sta spr_v_current_ptr+GOLFER_CLUB_SHAFT_SPR_NUM   
    ; Set positions.
    lda GOLFER_UPPER_BODY_POS_X
    sta spr_v_x_lo+GOLFER_UPPER_BODY_SPR_NUM
    lda GOLFER_UPPER_BODY_POS_Y
    sta spr_v_y+GOLFER_UPPER_BODY_SPR_NUM
    lda GOLFER_LOWER_BODY_POS_X
    sta spr_v_x_lo+GOLFER_LOWER_BODY_SPR_NUM
    lda #GOLFER_BASE_Y+21
    sta spr_v_y+GOLFER_LOWER_BODY_SPR_NUM
    lda GOLFER_CLUB_X
    sta spr_v_x_lo+GOLFER_CLUB_SHAFT_SPR_NUM 
    lda GOLFER_CLUB_Y
    sta spr_v_y+GOLFER_CLUB_SHAFT_SPR_NUM 

    lda #golfer_c_STATE_READY
    sta golfer_v_current_state

    ; We borrow this to control modification in-game options.
    lda #1
    sta golfer_keyboard_locked

    rts ; EXIT POINT.

.putt
    ; Initial set-up for putting.  First the sprite data.
    lda #sprd_c_GOLFER_PUTT_UPPER 
    sta spr_v_current_ptr+GOLFER_UPPER_BODY_SPR_NUM   
    lda #sprd_c_GOLFER_SWING_LOWER 
    sta spr_v_current_ptr+GOLFER_LOWER_BODY_SPR_NUM   
    lda #sprd_c_GOLFER_PUTT_CLUB
    sta spr_v_current_ptr+GOLFER_CLUB_SHAFT_SPR_NUM   
    ; Now the positions.
    lda #GOLFER_PUTT_POS_X 
    sta spr_v_x_lo+GOLFER_UPPER_BODY_SPR_NUM
    sta spr_v_x_lo+GOLFER_LOWER_BODY_SPR_NUM
    lda #GOLFER_PUTT_POS_Y+3 
    sta spr_v_y+GOLFER_UPPER_BODY_SPR_NUM
    lda #GOLFER_PUTT_POS_Y+21 
    sta spr_v_y+GOLFER_LOWER_BODY_SPR_NUM
    lda #GOLFER_PUTT_CLUB_SHAFT_OFFSETS_X
    sta spr_v_x_lo+GOLFER_CLUB_SHAFT_SPR_NUM 
    lda #GOLFER_PUTT_CLUB_SHAFT_OFFSETS_Y
    sta spr_v_y+GOLFER_CLUB_SHAFT_SPR_NUM 

    lda #golfer_c_STATE_READY
    sta golfer_v_current_state

    rts
; end sub golfer_setup_draw
} ; !zone

; **************************************************

!zone {
golfer_init2
    ; There are some global settings.
    ; Position of sprites #0 and #1 never changes and they're both
    ; multicolour.  We can also set the two global colours here -
    ; light red and red.
    lda #LIGHT_RED
    sta SPMC0
    lda #RED
    sta SPMC1

    ; Set positions.
    ; NOTE: all X-positions have high byte=0.
    lda #0
    sta spr_v_x_hi+GOLFER_UPPER_BODY_SPR_NUM
    sta spr_v_x_hi+GOLFER_LOWER_BODY_SPR_NUM
    sta spr_v_x_hi+GOLFER_CLUB_SHAFT_SPR_NUM 
    sta spr_v_hires+GOLFER_UPPER_BODY_SPR_NUM
    sta spr_v_hires+GOLFER_LOWER_BODY_SPR_NUM
    sta spr_v_hires+GOLFER_CLUB_SHAFT_SPR_NUM 

    lda SPENA
    ora #1<<GOLFER_POS_MARKER_SPR_NUM
    sta SPENA
    lda #sprd_c_CROSSHAIR
    sta spr_v_current_ptr+GOLFER_POS_MARKER_SPR_NUM
    lda #GOLFER_POS_MARKER_X 
    sta spr_v_x_lo+GOLFER_POS_MARKER_SPR_NUM
    lda #0
    sta spr_v_x_hi+GOLFER_POS_MARKER_SPR_NUM
    lda #GOLFER_POS_MARKER_Y 
    sta spr_v_y+GOLFER_POS_MARKER_SPR_NUM
    lda #GOLFER_POS_MARKER_VX_LO
    sta spr_v_vx_lo+GOLFER_POS_MARKER_SPR_NUM 
    lda #GOLFER_POS_MARKER_VX_HI
    sta spr_v_vx_hi+GOLFER_POS_MARKER_SPR_NUM 

    rts
; end sub golfer_init2
} ; !zone

; **************************************************

!zone {
golfer_init_watching_shot1
    lda #golfer_c_STATE_FINISHED
    sta golfer_v_current_state

    lda #sprd_c_GOLFER_WATCHING_SWING_UPPER
    sta spr_v_current_ptr+GOLFER_UPPER_BODY_SPR_NUM
    lda #sprd_c_GOLFER_WATCHING_SWING_CLUB
    sta spr_v_current_ptr+GOLFER_CLUB_SHAFT_SPR_NUM
    lda #GOLFER_WATCHING_UPPER_OFFSET_X
    sta spr_v_x_lo+GOLFER_UPPER_BODY_SPR_NUM
    lda #GOLFER_WATCHING_UPPER_OFFSET_Y 
    sta spr_v_y+GOLFER_UPPER_BODY_SPR_NUM
    lda #GOLFER_WATCHING_CLUB_OFFSET_X 
    sta spr_v_x_lo+GOLFER_CLUB_SHAFT_SPR_NUM
    lda #GOLFER_WATCHING_CLUB_OFFSET_Y 
    sta spr_v_y+GOLFER_CLUB_SHAFT_SPR_NUM 

    rts
; end sub golfer_init_watching_shot1
} ; !zone

; **************************************************

; OUTPUTS:  C flag set if should notify player of distance; otherwise clear.
!zone {
golfer_s_record_distance
    +clr round_v_current_player_is_on_green

    ; Whatever happens later, we'll always need to add a shot.
    ; Update the display afterwards as well.
    ldx round_v_current_player
    inc players_v_current_shots,x

    ; No need to calculate distance if the ball's already in the hole!
    lda ball_v_is_in_hole
    +branch_if_false +

    jsr golfer_s_evaluate_hole

    ; Write our score to the score card UNLESS MATCH PLAY!
    lda shared_v_is_match_play
    +branch_if_true .dont_write
    ldx round_v_current_player
    jsr sc_s_write
.dont_write
    ; We record the fact that the current player has finished the hole by 
    ; setting their 'current_shots' value to $ff (i.e. negative).
    ldx round_v_current_player
    lda #$ff
    sta players_v_current_shots,x
    ; Display this golfer's updated score straight away (unless match play).
    ; NOTE: X already holds player #.
    lda shared_v_is_match_play
    +branch_if_true .skip_display_score
    lda #1
    sta P0
    sta P1
    jsr sc_s_draw_name_and_score
.skip_display_score
    jsr stats_s_inc_balls_holed
    clc
    rts ; EXIT POINT.

+
    ; If ball's in water or out of bounds it hasn't moved!
    lda ball_v_current_terrain
    cmp #ball_c_TERRAIN_WATER
    beq .no_distance
    lda ball_v_out_of_bounds    
    +branch_if_false +
.no_distance
    clc
    rts ; EXIT POINT.

+
    ; NOTE: X still holds value of round_v_current_player.
    lda target_z_lo
    sta players_v_distance_lo,x
    lda target_z_hi
    sta players_v_distance_hi,x
    lda ball_v_current_terrain
    sta players_v_terrain,x

    ; NOTE: terrain in accumulator.
    cmp #ball_c_TERRAIN_GREEN_FWAY
    bne .end
    lda target_z_lo
    cmp #round_c_MAX_PUTT_DISTANCE_LO 
    lda target_z_hi
    sbc #round_c_MAX_PUTT_DISTANCE_HI 
    bcs .end
    inc round_v_current_player_is_on_green

.end
    sec
    rts
; end sub golfer_s_record_distance
} ; !zone

; **************************************************

; Enable the sprite and reset its x-position to the center of
; the screen.
!zone {
golfer_reset_crosshair
    lda SPENA
    ora #1<<GOLFER_POS_MARKER_SPR_NUM 
    sta SPENA

    ldx #golfer_c_MARKER_INITIAL_INDEX
    stx golfer_v_marker_index
    lda golfer_l_MARKER_X_POS2,x
    sta spr_v_x_lo+GOLFER_POS_MARKER_SPR_NUM 
    lda #0
    sta spr_v_x_hi+GOLFER_POS_MARKER_SPR_NUM 
    lda golfer_l_MARKER_Y_POS2,x
    sta spr_v_y+GOLFER_POS_MARKER_SPR_NUM 

    lda #sprd_c_CROSSHAIR
    sta spr_v_current_ptr+GOLFER_POS_MARKER_SPR_NUM 

    lda #1
    sta golfer_v_crosshair_active
    lda #GOLFER_POS_MARKER_VX_LO
    sta spr_v_vx_lo+GOLFER_POS_MARKER_SPR_NUM 

    rts
; end sub golfer_reset_crosshair
} ; !zone

; **************************************************

!zone {
golfer_s_update1_preswing
    lda shared_v_is_match_play
    +branch_if_false +
    jsr golfer_s_update1_concede_query
    bcc +
    rts ; EXIT POINT.

+
    ldx joy_v_current_port
    lda round_v_must_putt
    +branch_if_true .check_hide_golfer

    ; Swing not yet initiated - player may position crosshair and select club.
    +joy_m_is_up
    bne .check_down
    +joy_m_is_locked_up
    beq +
    rts ; EXIT POINT - tried 'up' but locked.
+
    +joy_m_lock_up
    jsr clubs_s_prev
    bcs +
    jsr clubs_s_draw2
+
    rts ; EXIT POINT - previous club selected.

.check_down
    +joy_m_is_down
    bne .unlock
    +joy_m_is_locked_down
    +branch_if_true .end
    +joy_m_lock_down
    jsr clubs_s_next
    bcs +
    jsr clubs_s_draw2
+
    rts ; EXIT POINT - next club selected.

.unlock
    +joy_m_release_vertical

.check_horizontal
    ; Check horizontal.
    +joy_m_is_left
    bne .check_right
    ldx #spr_c_LEFT
    jsr golfer_s_init_marker_move
    rts ; EXIT POINT.
    
.check_right
    +joy_m_is_right
    bne .crosshair_still
    ldx #spr_c_RIGHT
    jsr golfer_s_init_marker_move
    rts ; EXIT POINT.

.crosshair_still
    lda #$ff
    sta golfer_pos_marker_direction

    ; No directional input.  Does player wish to intiate swing?
    +joy_m_is_fire
    bne .unlock_fire
    +joy_m_is_locked_fire
    +branch_if_true .end
    lda #golfer_c_STATE_POWERING_UP
    sta golfer_v_current_state
    +clr golfer_v_crosshair_active
    ; FIXME: hack to hide crosshair!
    lda #0
    sta SP0Y+(2*GOLFER_CROSSHAIR_SPR_NUM) 

    rts ; EXIT POINT.

.check_hide_golfer
    jsr golfer_s_check_hidden
    jmp .check_horizontal

.unlock_fire
    +joy_m_release_fire

.end
    rts
; end sub golfer_update1_preswing
} ; !zone

; **************************************************

!zone {
golfer_s_update1_finished
    ; Nothing to do unless the ball's stopped and round-manager is
    ; WAITING_FOR_NEXT.
    lda ball_v_current_state
    bne .not_ready
    lda round_current_state
    cmp #ROUND_STATE_WAITING_FOR_NEXT
    beq +
.not_ready    
    rts ; EXIT POINT.

+
    ldx joy_v_current_port
    ; We're just listening out for the fire button.
    +joy_m_is_fire
    bne .unlock
    +joy_m_is_locked_fire
    +branch_if_true .end
    jsr round_handle_shot_complete
    jmp .end
.unlock
    +joy_m_release_fire
.end
    rts
; end sub golfer_s_update1_finished
} ; !zone

; **************************************************

!zone {
golfer_update1_putt_power
    lda golfer_shot_power_finalized
    +branch_if_true .end

    jsr powarc_s_update

    ldx joy_v_current_port
    +joy_m_is_fire
    bne .finalize
    ; Player is still pressing button, so increment shot power.
    lda golfer_v_shot_power
    clc
    adc #GOLFER_PUTT_POWER_INCREMENT
    sta golfer_v_shot_power
    rts ; EXIT POINT.

.finalize
    inc golfer_shot_power_finalized
    +golfer_set_club_color GOLFER_CLUB_NORMAL_COLOR

.end
    rts
; end sub golfer_update1_putt_power
} ; !zone

; **************************************************

!zone {
golfer_s_update_looking_up
    dec golfer_look_up_count
    bne .end

    lda round_v_must_putt
    +branch_if_true .putt

    ; So swing...
    lda GOLFER_UPPER_BODY_POS_Y
    sta spr_v_y+GOLFER_UPPER_BODY_SPR_NUM   
    lda #sprd_c_GOLFER_SWING_UPPER 
    bne +

.putt
    ; FIXME: a bit messy!  Could alternatively set a variable somewhere?!
    ; If the golfer is hidden, update looking up/down as usual but write the
    ; new sprite pointer to the buffer, rather than to the sprite engine.  In
    ; this way, we'll see the change when the sprite pointers are restored.
    lda #sprd_c_GOLFER_PUTT_UPPER
    ldx golfer_v_is_hidden
    beq +
    sta golfer_v_spr_pointer_buffer+1
    bne ++
+
    sta spr_v_current_ptr+GOLFER_UPPER_BODY_SPR_NUM   
++
    lda #golfer_c_STATE_READY
    sta golfer_v_current_state
    +clr golfer_look_up_count

.end
    rts
; end sub golfer_s_update_looking_up
} ; !zone

; **************************************************

!zone {
golfer_s_update_looking_down
    jsr rand_s_get
    cmp #$40
    bcs .end
    inc golfer_look_up_count
    lda golfer_look_up_count
    cmp #50
    bcc .end

    lda round_v_must_putt
    +branch_if_true .putt

    ; So swing...
    lda #GOLFER_UPPER_BODY_LOOKING_UP_POS_Y 
    sta spr_v_y+GOLFER_UPPER_BODY_SPR_NUM   
    lda #sprd_c_GOLFER_LOOKING_UP  
    bne +

.putt
    ; So, putt...
    lda #sprd_c_GOLFER_PUTT_LOOKING_UP
    ldx golfer_v_is_hidden
    beq +
    sta golfer_v_spr_pointer_buffer+1
    bne ++
+
    sta spr_v_current_ptr+GOLFER_UPPER_BODY_SPR_NUM   
++
    lda #golfer_c_STATE_LOOKING_UP
    sta golfer_v_current_state

.end
    rts
; end sub golfer_s_update_looking_down
} ; !zone

; **************************************************

!zone {
golfer_init_poised
    lda #GOLFER_HOLD_TIME*5
    sta golfer_v_frame_count
    inc golfer_v_current_state

    ; Set up second half of putting animation.
    lda #sprd_c_GOLFER_PUTT_UPPER+sprd_c_GOLFER_PUTT_BACKSWING_FRAMES-1
    sta spr_v_anim_start_ptr+GOLFER_UPPER_BODY_SPR_NUM   
    lda #sprd_c_GOLFER_PUTT_CLUB+sprd_c_GOLFER_PUTT_BACKSWING_FRAMES-1 
    sta spr_v_anim_start_ptr+GOLFER_CLUB_SHAFT_SPR_NUM
    lda #sprd_c_GOLFER_PUTT_UPPER
    sta spr_v_anim_end_ptr+GOLFER_UPPER_BODY_SPR_NUM   
    lda #sprd_c_GOLFER_PUTT_CLUB
    sta spr_v_anim_end_ptr+GOLFER_CLUB_SHAFT_SPR_NUM
    lda #0
    sta spr_v_anim_seq_inc+GOLFER_UPPER_BODY_SPR_NUM   
    sta spr_v_anim_seq_inc+GOLFER_CLUB_SHAFT_SPR_NUM
    lda #GOLFER_PUTTING_FRAME_RATE
    sta spr_v_anim_timer+GOLFER_UPPER_BODY_SPR_NUM   
    sta spr_v_anim_timer+GOLFER_CLUB_SHAFT_SPR_NUM
    sta spr_v_framerate+GOLFER_UPPER_BODY_SPR_NUM   
    sta spr_v_framerate+GOLFER_CLUB_SHAFT_SPR_NUM
    ; Index into club shaft position offsets table.
    lda #3
    sta golfer_anim_frame   

    rts
; end sub golfer_init_poised
} ; !zone

; **************************************************

!zone {
golfer_store_new_ball_pos
    ldx round_v_current_player
    lda ball_world_x_lo
    sta players_v_ball_pos_x_lo,x
    lda ball_world_x_hi
    sta players_v_ball_pos_x_hi,x
    lda ball_world_z_lo
    sta players_v_ball_pos_z_lo,x
    lda ball_world_z_hi
    sta players_v_ball_pos_z_hi,x
    rts
; end sub golfer_store_new_ball_pos
} ; !zone

; **************************************************

; OUTPUTS:  C flag set if animation complete; otherwise clear.
!zone {
golfer_s_animate_swing
    ldx #GOLFER_CLUB_SHAFT_SPR_NUM   
    jsr spr_s_animate_onetime
    ldx #GOLFER_UPPER_BODY_SPR_NUM   
    jsr spr_s_animate_onetime
    ldx #GOLFER_LOWER_BODY_SPR_NUM   
    jsr spr_s_animate_onetime
    bcs .animation_complete

    ; NOTE: following flag is set if we advanced to the next frame in
    ; latest call to spr_s_animate_onetime.
    lda spr_v_is_next_frame
    +branch_if_false .still_animating

    inc golfer_anim_frame
    lda golfer_anim_frame
    pha
    cmp #7
    bne +
    ldy #sfx_c_SWISH
    jsr snd_s_init_sfx
+
    pla
    tax
    lda GOLFER_UPPER_BODY_POS_X,x
    sta spr_v_x_lo+GOLFER_UPPER_BODY_SPR_NUM   
    lda GOLFER_UPPER_BODY_POS_Y,x
    sta spr_v_y+GOLFER_UPPER_BODY_SPR_NUM   
    lda GOLFER_LOWER_BODY_POS_X,x
    sta spr_v_x_lo+GOLFER_LOWER_BODY_SPR_NUM   
    lda GOLFER_CLUB_X,x
    sta spr_v_x_lo+GOLFER_CLUB_SHAFT_SPR_NUM   
    lda GOLFER_CLUB_Y,x
    sta spr_v_y+GOLFER_CLUB_SHAFT_SPR_NUM   

    ; Must the ball be launched (and particle system started)?
    lda spr_v_current_ptr+GOLFER_UPPER_BODY_SPR_NUM
    cmp #sprd_c_GOLFER_SWING_UPPER+sprd_c_GOLFER_CONTACT_OFFSET 
    bne .still_animating
    sta ball_v_must_launch

.still_animating
    clc
    rts

.animation_complete
    rts
; end sub golfer_s_animate_swing
} ; !zone
; **************************************************

; OUTPUTS:  C flag set if animation complete; else clear.
!zone {
golfer_advance_putting_animation
    ldx #GOLFER_UPPER_BODY_SPR_NUM
    jsr spr_s_animate_onetime
    ldx #GOLFER_CLUB_SHAFT_SPR_NUM
    jsr spr_s_animate_onetime
    bcc +
    rts ; EXIT POINT - animation sequence has ended.

+
    lda spr_v_is_next_frame
    +branch_if_false .end
    
    ; Check if ball needs to be launched (- i.e. club head has come into
    ; contact with it)...
    lda golfer_v_current_state
    cmp #golfer_c_STATE_ANIMATE_FORESWING_PUTT
    bne .end
    lda ball_v_current_state
    cmp #ball_c_STATE_STATIONARY
    bne .end
    lda spr_v_current_ptr+GOLFER_UPPER_BODY_SPR_NUM
    cmp #sprd_c_GOLFER_PUTT_UPPER+sprd_c_GOLFER_PUTT_CONTACT_OFFSET
    bne .end
    jsr ball_s_launch_putt

.end
    ; Make sure C flag clear so animation continues.
    clc
    rts
; end sub golfer_advance_putting_animation
} ; !zone

; **************************************************

!zone {
golfer_s_update1_powering_up
    ldx joy_v_current_port

    lda powarc_v_power_maxed_out
    +branch_if_true .hold

    ; Fire-button release?
    +joy_m_is_fire
    beq .end

.hold
    +golfer_m_init_hold golfer_c_HOLD_DURATION,golfer_c_STATE_HOLD
    lda powarc_v_step_count
    sta powarc_v_power_offset
    jsr powarc_s_init_max_animation
    ; X may be trashed so load again.
    ldx joy_v_current_port
    +joy_m_lock_fire
    ; If this is a putt, need to send msg to ball module that initial velocity
    ; now needs to be calculated.  In case of swing, still need to set
    ; precision.
    lda round_v_must_putt
    +branch_if_false .end
    lda #ball_c_STATE_MUST_INIT
    sta ball_v_current_state

.end
    rts
; end sub golfer_s_update1_powering_up
} ; !zone

; **************************************************

!zone {
golfer_s_update0_hold
    dec golfer_v_frame_count
    bne .end

    ; Next stage depends on whether this is a swing or putt.
    lda round_v_must_putt
    +branch_if_true .putt

    ; So swing:
    lda #golfer_c_STATE_PRECISION
    sta golfer_v_current_state
    jsr powarc_s_init_precision
    rts ; EXIT POINT.

.putt
    lda #golfer_c_STATE_ANIMATE_BACKSWING_PUTT
    sta golfer_v_current_state
    jsr golfer_s_init_putt_animation

.end
    rts
; end sub golfer_s_update0_hold
} ; !zone

; **************************************************

!zone {
golfer_s_update1_precision
    ldx joy_v_current_port  

    lda powarc_v_precision_ended  
    +branch_if_true .hold

    +joy_m_is_fire 
    bne .unlock
    +joy_m_is_locked_fire
    +branch_if_true .end

.hold
    inc golfer_v_shot_spin_set 
    lda powarc_v_step_count   
    sta powarc_v_precision_offset 
    +golfer_m_init_hold golfer_c_HOLD_DURATION,golfer_c_STATE_HOLD2
    ; Message to ball module that trajectory must be initialized.
    lda #ball_c_STATE_MUST_INIT
    sta ball_v_current_state
    rts ; EXIT POINT.

.unlock
    +joy_m_release_fire

.end
    rts
; end sub golfer_s_update1_precision
} ; !zone

; **************************************************

!zone {
golfer_s_update0_hold2
    dec golfer_v_frame_count
    bne .end
    ; Initialize the golfer animation.
    ; TODO: full or half swing?  Should depend on club & power.
    lda #golfer_c_STATE_ANIMATE_BACKSWING
    sta golfer_v_current_state
    jsr golfer_s_init_swing_animation
.end
    rts
; end sub golfer_s_update0_hold2
} ; !zone

; **************************************************

!zone {
golfer_s_init_animate_hold
    +golfer_m_init_hold golfer_c_HOLD_DURATION_SWING,golfer_c_STATE_ANIMATE_HOLD
    ; Prepare sprite animation.
    ; Torso.
    lda #sprd_c_GOLFER_SWING_UPPER+sprd_c_GOLFER_MIDSWING_OFFSET 
    sta spr_v_anim_start_ptr+GOLFER_UPPER_BODY_SPR_NUM    
    lda #sprd_c_GOLFER_SWING_UPPER+sprd_c_GOLFER_MIDSWING_OFFSET+sprd_c_GOLFER_FORESWING_FRAMES-1 
    sta spr_v_anim_end_ptr+GOLFER_UPPER_BODY_SPR_NUM        
    ; Legs.
    lda #sprd_c_GOLFER_SWING_LOWER+sprd_c_GOLFER_MIDSWING_OFFSET 
    sta spr_v_anim_start_ptr+GOLFER_LOWER_BODY_SPR_NUM    
    lda #sprd_c_GOLFER_SWING_LOWER+sprd_c_GOLFER_MIDSWING_OFFSET+sprd_c_GOLFER_FORESWING_FRAMES-1
    sta spr_v_anim_end_ptr+GOLFER_LOWER_BODY_SPR_NUM        
    ; Club.
    lda #sprd_c_GOLFER_SWING_CLUB+sprd_c_GOLFER_MIDSWING_OFFSET 
    sta spr_v_anim_start_ptr+GOLFER_CLUB_SHAFT_SPR_NUM    
    lda #sprd_c_GOLFER_SWING_CLUB+sprd_c_GOLFER_MIDSWING_OFFSET+sprd_c_GOLFER_FORESWING_FRAMES-1
    sta spr_v_anim_end_ptr+GOLFER_CLUB_SHAFT_SPR_NUM        

    lda #1
    sta spr_v_anim_seq_inc+GOLFER_CLUB_SHAFT_SPR_NUM           
    sta spr_v_anim_seq_inc+GOLFER_UPPER_BODY_SPR_NUM        
    sta spr_v_anim_seq_inc+GOLFER_LOWER_BODY_SPR_NUM        

    lda #GOLFER_FORESWING_FRAME_RATE
    sta spr_v_anim_timer+GOLFER_CLUB_SHAFT_SPR_NUM                     
    sta spr_v_anim_timer+GOLFER_UPPER_BODY_SPR_NUM                 
    sta spr_v_anim_timer+GOLFER_LOWER_BODY_SPR_NUM                  
    sta spr_v_framerate+GOLFER_CLUB_SHAFT_SPR_NUM                                
    sta spr_v_framerate+GOLFER_UPPER_BODY_SPR_NUM                            
    sta spr_v_framerate+GOLFER_LOWER_BODY_SPR_NUM       

    rts
; end sub golfer_s_init_animate_hold
} ; !zone

; **************************************************

!zone {
golfer_s_update0_hold_animate
    dec golfer_v_frame_count
    bne .end
    lda #golfer_c_STATE_ANIMATE_FORESWING
    sta golfer_v_current_state

.end
    rts
; end sub golfer_s_update0_hold_animate
} ; !zone

; **************************************************

!zone {
golfer_s_update0_hold2_animate
    dec golfer_v_frame_count
    bne .end
    jsr golfer_init_watching_shot1
.end
    rts
; end sub golfer_s_update0_hold2_animate
} ; !zone

; **************************************************

!zone {
golfer_s_init_putt_animation
    lda #sprd_c_GOLFER_PUTT_UPPER 
    sta spr_v_current_ptr+GOLFER_UPPER_BODY_SPR_NUM   
    sta spr_v_anim_start_ptr+GOLFER_UPPER_BODY_SPR_NUM   
    lda #sprd_c_GOLFER_PUTT_CLUB
    sta spr_v_anim_start_ptr+GOLFER_CLUB_SHAFT_SPR_NUM   
    lda #sprd_c_GOLFER_PUTT_UPPER+sprd_c_GOLFER_PUTT_FORESWING_FRAMES_PT0-1  
    sta spr_v_anim_end_ptr+GOLFER_UPPER_BODY_SPR_NUM   
    lda #sprd_c_GOLFER_PUTT_CLUB+sprd_c_GOLFER_PUTT_FORESWING_FRAMES_PT0-1 
    sta spr_v_anim_end_ptr+GOLFER_CLUB_SHAFT_SPR_NUM   
    lda #1
    sta spr_v_anim_seq_inc+GOLFER_UPPER_BODY_SPR_NUM   
    sta spr_v_anim_seq_inc+GOLFER_CLUB_SHAFT_SPR_NUM   
    lda #GOLFER_PUTTING_FRAME_RATE 
    sta spr_v_anim_timer+GOLFER_UPPER_BODY_SPR_NUM   
    sta spr_v_anim_timer+GOLFER_CLUB_SHAFT_SPR_NUM   
    sta spr_v_framerate+GOLFER_UPPER_BODY_SPR_NUM   
    sta spr_v_framerate+GOLFER_CLUB_SHAFT_SPR_NUM   

    rts
; end sub golfer_s_init_putt_animation
} ; !zone

; **************************************************

!zone {
golfer_s_update0_hold_putt
    dec golfer_v_frame_count
    bne .end
    lda #golfer_c_STATE_ANIMATE_FORESWING_PUTT
    sta golfer_v_current_state
    jsr golfer_s_init_putt_animation_foreswing
.end
    rts
; end sub golfer_s_update0_hold_putt
} ; !zone

; **************************************************

!zone {
golfer_s_init_putt_animation_foreswing
    lda #sprd_c_GOLFER_PUTT_UPPER+sprd_c_GOLFER_PUTT_MIDSWING_OFFSET
    sta spr_v_current_ptr+GOLFER_UPPER_BODY_SPR_NUM   
    sta spr_v_anim_start_ptr+GOLFER_UPPER_BODY_SPR_NUM   
    lda #sprd_c_GOLFER_PUTT_CLUB+18
    sta spr_v_anim_start_ptr+GOLFER_CLUB_SHAFT_SPR_NUM   
    lda #sprd_c_GOLFER_PUTT_UPPER+9
    sta spr_v_anim_end_ptr+GOLFER_UPPER_BODY_SPR_NUM   
    lda #sprd_c_GOLFER_PUTT_CLUB+9
    sta spr_v_anim_end_ptr+GOLFER_CLUB_SHAFT_SPR_NUM   
    lda #1
    sta spr_v_anim_seq_inc+GOLFER_UPPER_BODY_SPR_NUM   
    sta spr_v_anim_seq_inc+GOLFER_CLUB_SHAFT_SPR_NUM   
    lda #GOLFER_PUTTING_FRAME_RATE 
    sta spr_v_anim_timer+GOLFER_UPPER_BODY_SPR_NUM   
    sta spr_v_anim_timer+GOLFER_CLUB_SHAFT_SPR_NUM   
    sta spr_v_framerate+GOLFER_UPPER_BODY_SPR_NUM   
    sta spr_v_framerate+GOLFER_CLUB_SHAFT_SPR_NUM   

    rts
; end sub golfer_s_init_putt_animation_foreswing
} ; !zone

; **************************************************

!zone {
golfer_s_rotate_direction
    lda #golfer_c_BASE_DIRECTION_X_LO 
    sta golfer_v_direction_x_lo 
    lda #golfer_c_BASE_DIRECTION_X_HI 
    sta golfer_v_direction_x_hi 
    lda #golfer_c_BASE_DIRECTION_Z_LO 
    sta golfer_v_direction_z_lo 
    lda #golfer_c_BASE_DIRECTION_Z_HI 
    sta golfer_v_direction_z_hi 

    lda #<golfer_v_direction_x_lo
    sta MATHS0
    lda #>golfer_v_direction_x_lo
    sta MATHS1
    lda #<golfer_v_direction_z_lo
    sta MATHS2
    lda #>golfer_v_direction_z_lo
    sta MATHS3
    lda hole_current_rotation_angle     
    sta MATHS4
    ; FIXME: why was it sec below?!
    sec
    jsr maths_s_rotate

    rts
; end sub golfer_s_rotate_direction
} ; !zone

; **************************************************

; INPUTS:   X = direction (spr_c_LEFT or spr_c_RIGHT)
!zone {
golfer_s_init_marker_move
    cpx golfer_pos_marker_direction
    beq .end    ; Already moving in this direction!
    lda golfer_l_MARKER_BOUNDS,x
    cmp golfer_v_marker_index
    beq .end    ; Can't move any further.
    stx golfer_pos_marker_direction
    ; Set the delay to 1 because we want the marker sprite to move 
    ; immediately on the next refresh.
    lda #1
    sta golfer_v_marker_move_count

.end
    rts
; end sub golfer_s_init_marker_move
} ; !zone

; **************************************************

!zone {
golfer_s_move_marker
    ldx golfer_pos_marker_direction
    bmi .pulse
    
    dec golfer_v_marker_move_count
    lda golfer_v_marker_move_count
    bne .pulse  ; Not time yet...

    ; Time to move.  X holds direction.
    lda golfer_v_marker_index
    clc
    adc golfer_l_MARKER_DELTA,x
    sta golfer_v_marker_index
    cmp golfer_l_MARKER_BOUNDS,x
    bne +
    ; Can't go any further after this so stop moving.
    ; FIXME: this may not make any difference!
    lda #$ff
    sta golfer_pos_marker_direction

+
    lda #golfer_c_MARKER_MOVE_DELAY
    sta golfer_v_marker_move_count
    ldx golfer_v_marker_index
    lda golfer_l_MARKER_X_POS2,x
    sta spr_v_x_lo+GOLFER_POS_MARKER_SPR_NUM                 
    lda golfer_l_MARKER_Y_POS2,x
    sta spr_v_y+GOLFER_POS_MARKER_SPR_NUM                 

.pulse
    ; NOTE: use same colors as overhead ball.
    lda spr_v_color+hole_c_OVERHEAD_BALL_SW_SPR_NUM
    sta spr_v_color+GOLFER_POS_MARKER_SPR_NUM 

    rts
; end sub golfer_s_move_marker
} ; !zone

; **************************************************

; NOTE: golfer should be hidden during pre-putt while joystick is DOWN...
; INPUTS:   X = current joystick port
!zone {
golfer_s_check_hidden
    +joy_m_is_down 
    beq .hide

    ; Joystick not down so show golfer if hidden.
    lda golfer_v_is_hidden
    +branch_if_false .end
    ; Restore pointers.
    lda golfer_v_spr_pointer_buffer
    sta spr_v_current_ptr+GOLFER_CLUB_SHAFT_SPR_NUM
    lda golfer_v_spr_pointer_buffer+1
    sta spr_v_current_ptr+GOLFER_UPPER_BODY_SPR_NUM
    lda golfer_v_spr_pointer_buffer+2
    sta spr_v_current_ptr+GOLFER_LOWER_BODY_SPR_NUM
    +clr golfer_v_is_hidden
    jsr golfer_s_restore_shadow
    rts ; EXIT POINT.

.hide
    lda golfer_v_is_hidden
    +branch_if_true .end
    ; Save existing pointers in buffer (so can be restored later).
    lda spr_v_current_ptr+GOLFER_CLUB_SHAFT_SPR_NUM
    sta golfer_v_spr_pointer_buffer 
    lda spr_v_current_ptr+GOLFER_UPPER_BODY_SPR_NUM
    sta golfer_v_spr_pointer_buffer+1 
    lda spr_v_current_ptr+GOLFER_LOWER_BODY_SPR_NUM
    sta golfer_v_spr_pointer_buffer+2 
    ; NOTE: particle sprite will be cleared, so draw that instead.
    lda #sprd_c_PARTICLES_SHOT
    sta spr_v_current_ptr+GOLFER_CLUB_SHAFT_SPR_NUM
    sta spr_v_current_ptr+GOLFER_UPPER_BODY_SPR_NUM
    sta spr_v_current_ptr+GOLFER_LOWER_BODY_SPR_NUM
    sta golfer_v_is_hidden
    jsr golfer_s_hide_shadow

.end
    rts
; end sub golfer_s_check_hidden
} ; !zone

; **************************************************

!zone {
golfer_s_init_shadow
    +utils_m_kernal_out

    ldx round_v_must_putt
    lda golfer_l_SHADOW_POS_LO,x
    sta P0
    lda golfer_l_SHADOW_POS_HI,x
    sta P1

    ldy #(16-1)
-
    lda (P0),y
    sta golfer_l_ANTI_SHADOW_BUFFER,y
    and golfer_l_SHADOW_PATTERN,y
    sta golfer_l_SHADOW_BUFFER,y
    sta (P0),y
    dey
    bpl -

    +utils_m_kernal_in

    rts
; end sub golfer_s_init_shadow
} ; !zone

; **************************************************

!zone {
golfer_s_hide_shadow
    ldx round_v_must_putt
    lda golfer_l_SHADOW_POS_LO,x
    sta P0
    lda golfer_l_SHADOW_POS_HI,x
    sta P1

    ldy #(16-1)
-
    lda golfer_l_ANTI_SHADOW_BUFFER,y
    sta (P0),y
    dey
    bpl -

    rts
; end sub golfer_s_hide_shadow
} ; !zone

; **************************************************

!zone {
golfer_s_restore_shadow
    ldx round_v_must_putt
    lda golfer_l_SHADOW_POS_LO,x
    sta P0
    lda golfer_l_SHADOW_POS_HI,x
    sta P1

    ldy #(16-1)
-
    lda golfer_l_SHADOW_BUFFER,y
    sta (P0),y
    dey
    bpl -

    rts
; end sub golfer_s_restore_shadow
} ; !zone

; **************************************************

; Based on number of shots and distance putted, see if golfer is 
; deserving of praise.
; INPUTS: X = current player.
!zone {
golfer_s_evaluate_hole
    lda hole_v_par
    sec
    sbc players_v_current_shots,x
    clc
    adc #2
    bpl .at_least_double_bogey

    ; So over par, but maybe was a good putt?
    lda players_v_distance_lo,x
    cmp #<golfer_c_MIN_PRAISEWORTHY_PUTT_FT
    lda players_v_distance_hi,x
    sbc #>golfer_c_MIN_PRAISEWORTHY_PUTT_FT
    bcs .praiseworthy_putt

    ; No praise this time!
    rts ; EXIT POINT.

.praiseworthy_putt
    ldx #playmsg_c_TYPE_PUTT
    bne +
.at_least_double_bogey
    tax
+
    jsr playmsg_s_praise_shot

    rts
; end sub golfer_s_evaluate_hole
} ; !zone

; **************************************************

; OUTPUT:   C flag set = player has requested to concede hole;
;           else C flag clear.
!zone {
golfer_s_check_concede
    jsr SCNKEY
    lda $cb
    cmp #63 ; Matrix code for [RUN/STOP].
    bne .no_request

    ; Player has requested to concede hole.
    +set golfer_v_concede_query_active
    ldx #msg_c_CONCEDE_HOLE          
    jsr msg_s_display_stock_msg
    lda #golfer_c_RESPONSE_YES
    sta golfer_v_current_response
    jsr golfer_s_toggle_concede_response
    sec
    rts ; EXIT POINT.

.no_request
    clc
    rts
; end sub golfer_s_check_concede
} ; !zone

; **************************************************

!zone {
golfer_s_toggle_concede_response
    lda golfer_v_current_response
    eor #$01
    sta golfer_v_current_response
    pha

    ; First color both options in normal color.
    ldx #14
    lda #golfer_c_RESPONSE_GHOST_COLOR
-
    sta gfxs_c_DISPLAY_BASE+(24*40),x
    inx
    cpx #20
    bne -

    ; And now highlight current response.
    pla
    tay
    ldx golfer_l_RESPONSE_OFFSET_BEGIN,y
    lda golfer_l_RESPONSE_OFFSET_END,y
    sta MATHS0
    lda #golfer_c_RESPONSE_HIGHLIGHT_COLOR
-
    sta gfxs_c_DISPLAY_BASE+(24*40),x
    inx
    cpx MATHS0
    bne -

    rts
; end sub golfer_s_toggle_concede_response
} ; !zone

; **************************************************

!zone {
golfer_s_handle_concede_response
    ; These happen irrespective of response!
    jsr golfer_s_repair_msg_area
    +clr golfer_v_concede_query_active

    lda golfer_v_current_response
    cmp #golfer_c_RESPONSE_NO
    beq .no

    ; So, yes...
    ; Ensure that the other team wins.
    ; First set negative bit on all 'current_shots' variables to indicate
    ; that they've finished the hole.
    ldx #players_c_MAX_N-1
-
    lda players_v_current_shots,x
    ora #$80
    sta players_v_current_shots,x
    dex
    bpl -
    ; Indices 2 and 3 of 'sc_v_round_scores' are used to store running total
    ; of teams shots.  Set both to 0, then increment for current team.
    lda #0
    sta sc_v_round_scores+2
    sta sc_v_round_scores+3
    ldy round_v_current_player
    ldx shared_v_team_membership,y
    inc sc_v_round_scores+2,x
    ; Can now call this routine to end the hole with appropriate score.
    jsr round_handle_shot_complete
    ; FIXME: a bit of a hack - but if don't put this here, input will still be
    ; processed for player's shot.
    lda #golfer_c_STATE_IN_LIMBO                 
    sta golfer_v_current_state
    rts ; EXIT POINT.

.no
    jsr clubs_s_draw_distance

    rts
; end sub golfer_s_handle_concede_response
} ; !zone

; **************************************************

; OUTPUTS:  C flag clear - no query in progress; C flag set - query in progress
;           so don't check controls further after this.
!zone {
golfer_s_update1_concede_query
    ; If no query active, check if there should be.
    lda golfer_v_concede_query_active
    +branch_if_true .check_controls
    jsr golfer_s_check_concede
    ; NOTE: C flag set as appropriate by above routine.
    rts ; EXIT POINT.

.check_controls
    ldx joy_v_current_port

    +joy_m_is_fire
    bne .check_left
    +joy_m_is_locked_fire
    bne .end
    +joy_m_lock_fire
    jsr golfer_s_handle_concede_response
    sec
    rts ; EXIT POINT.

.check_left
    +joy_m_is_left
    bne .check_right
    ; React only if left is unlocked & current response is YES (= 1).
    +joy_m_is_locked_left
    bne .end
    +joy_m_lock_left
    lda golfer_v_current_response
    beq .end
    jsr golfer_s_toggle_concede_response
    sec
    rts ; EXIT POINT.
    
.check_right
    +joy_m_is_right
    bne .unlock
    ; React only if right is unlocked & current response is NO (= 0).
    +joy_m_is_locked_right
    bne .end
    +joy_m_lock_right
    lda golfer_v_current_response
    bne .end
    jsr golfer_s_toggle_concede_response
    sec
    rts ; EXIT POINT.

.unlock
    +joy_m_release_horizontal

.end
    sec
    rts
; end sub golfer_s_update1_concede_query
} ; !zone

; **************************************************

; Small helper routine.
!zone {
golfer_s_repair_msg_area
    jsr msg_s_clear

    lda #GREY3
    ldx #14
-
    sta gfxs_c_DISPLAY_BASE+(24*40),x
    inx
    cpx #20
    bne -

    rts
; end sub golfer_s_repair_msg_area
} ; !zone

; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************

golfer_c_SIZE = *-golfer_c_BEGIN

