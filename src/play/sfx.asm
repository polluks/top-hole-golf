; Top-hole Golf
; Copyright 2020 Matthew Clarke


sfx_c_BEGIN = *


; *****************
; *** CONSTANTS ***
; *****************
; Data tables for sound effects:
; FL,FH,PL,PH,[placeholder],AD,SR,WV,<frames>, 
;   FILTER-MODE,RESONANCE,CUTOFF-LO,CUTOFF-HI,
;   <must loop>
; Initial value of $ff means end sound effect.
sfx_l_BROWSE_INIT       !byte   195,16, 0,0, 0, $22,$f2, $11, 2, 0,0,0,0, 0
sfx_l_BROWSE_DATA       !byte   30,25,2, $ff,$10
sfx_l_BOUNCE_INIT       !byte   71,6, 0,0, 0, $22,$f8, $11, 2, 0,0,0,0, 0
sfx_l_BOUNCE_DATA       !byte   233,7,2, 97,8,2, $ff,$10
sfx_l_SWISH_INIT        !byte   97,8, 0,0, 0, $b1,$f6, $81, 2, 0,0,0,0, 0
sfx_l_SWISH_DATA        !byte   143,10,2, 195,16,2, 31,21,2, 135,33,2, 62,42,2
                        !byte   15,67,2, 125,84,2, $ff,$80

sfx_l_BALL_CLUB_INIT    !byte   135,33, 0,0, 0, $32,$f3, $11, 2
                        !byte   snd_c_FILTER_LOWPASS,7,$00,$10,0   
sfx_l_BALL_CLUB_DATA    !byte   135,33,2, $ff,$10
sfx_l_BALL_CUP_INIT     !byte   30,25, 0,0, 0, $23,$f7, $11, 2, 0,0,0,0, 0
sfx_l_BALL_CUP_DATA     !byte   45,1,1, 30,25,2, 45,1,1, 30,25,2, $ff,$10
sfx_l_SPLASH_INIT       !byte   209,18, 0,0, 0, $63,$fa, $81, 2, 0,0,0,0, 0   
sfx_l_SPLASH_DATA       !byte   $ff,$80
sfx_l_BALL_TREE_INIT    !byte   24,14, 0,0, 0, $22,$f2, $11, 2, 0,0,0,0, 0
sfx_l_BALL_TREE_DATA    !byte   $ff,$10
sfx_l_MAX_POWER_INIT    !byte   195,16, 0,0, 0, $33,$f9, $11, 2, 0,0,0,0, 0
sfx_l_MAX_POWER_DATA    !byte   209,18,2, 96,22,2, 49,28,2, 135,33,2, $ff,$10

sfx_c_BROWSE    = 0
sfx_c_BOUNCE    = 1
sfx_c_SWISH     = 2
sfx_c_BALL_CLUB = 3
sfx_c_BALL_CUP  = 4
sfx_c_SPLASH    = 5
sfx_c_BALL_TREE = 6
sfx_c_MAX_POWER = 7

sfx_l_INIT_ADDR_LO
    !byte   <sfx_l_BROWSE_INIT
    !byte   <sfx_l_BOUNCE_INIT
    !byte   <sfx_l_SWISH_INIT
    !byte   <sfx_l_BALL_CLUB_INIT
    !byte   <sfx_l_BALL_CUP_INIT
    !byte   <sfx_l_SPLASH_INIT
    !byte   <sfx_l_BALL_TREE_INIT    
    !byte   <sfx_l_MAX_POWER_INIT
sfx_l_INIT_ADDR_HI
    !byte   >sfx_l_BROWSE_INIT
    !byte   >sfx_l_BOUNCE_INIT
    !byte   >sfx_l_SWISH_INIT
    !byte   >sfx_l_BALL_CLUB_INIT
    !byte   >sfx_l_BALL_CUP_INIT
    !byte   >sfx_l_SPLASH_INIT
    !byte   >sfx_l_BALL_TREE_INIT    
    !byte   >sfx_l_MAX_POWER_INIT
sfx_l_DATA_ADDR_LO
    !byte   <sfx_l_BROWSE_DATA
    !byte   <sfx_l_BOUNCE_DATA
    !byte   <sfx_l_SWISH_DATA
    !byte   <sfx_l_BALL_CLUB_DATA
    !byte   <sfx_l_BALL_CUP_DATA
    !byte   <sfx_l_SPLASH_DATA
    !byte   <sfx_l_BALL_TREE_DATA
    !byte   <sfx_l_MAX_POWER_DATA
sfx_l_DATA_ADDR_HI
    !byte   >sfx_l_BROWSE_DATA
    !byte   >sfx_l_BOUNCE_DATA
    !byte   >sfx_l_SWISH_DATA
    !byte   >sfx_l_BALL_CLUB_DATA
    !byte   >sfx_l_BALL_CUP_DATA
    !byte   >sfx_l_SPLASH_DATA
    !byte   >sfx_l_BALL_TREE_DATA
    !byte   >sfx_l_MAX_POWER_DATA


; *****************
; *** VARIABLES ***
; *****************


; *******************
; ****** MACROS *****
; *******************


; *******************
; *** SUBROUTINES ***
; *******************

; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************

sfx_c_SIZE = *-sfx_c_BEGIN 

