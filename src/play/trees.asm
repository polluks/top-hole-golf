; Top-hole Golf
; Copyright 2020 Matthew Clarke


trees_c_BEGIN = *


; *****************
; *** CONSTANTS ***
; *****************
trees_c_MAX_N = 80
trees_c_MAX_DISTANCE_LO = <(200*hole_c_PIXELS_PER_YARD)
trees_c_MAX_DISTANCE_HI = >(200*hole_c_PIXELS_PER_YARD)

; Distance from ball determines appearance.
trees_l_DIST_LO
    !byte   <(5*hole_c_PIXELS_PER_YARD)
    !byte   <(8*hole_c_PIXELS_PER_YARD)
    !byte   <(11*hole_c_PIXELS_PER_YARD)
    !byte   <(14*hole_c_PIXELS_PER_YARD)
    !byte   <(18*hole_c_PIXELS_PER_YARD)
    !byte   <(22*hole_c_PIXELS_PER_YARD)
    !byte   <(30*hole_c_PIXELS_PER_YARD)
    !byte   <(50*hole_c_PIXELS_PER_YARD)
    !byte   <(80*hole_c_PIXELS_PER_YARD)
    !byte   <(130*hole_c_PIXELS_PER_YARD)
;    !byte   <(170*hole_c_PIXELS_PER_YARD)
trees_l_DIST_HI
    !byte   >(5*hole_c_PIXELS_PER_YARD)
    !byte   >(8*hole_c_PIXELS_PER_YARD)
    !byte   >(11*hole_c_PIXELS_PER_YARD)
    !byte   >(14*hole_c_PIXELS_PER_YARD)
    !byte   >(18*hole_c_PIXELS_PER_YARD)
    !byte   >(22*hole_c_PIXELS_PER_YARD)
    !byte   >(30*hole_c_PIXELS_PER_YARD)
    !byte   >(50*hole_c_PIXELS_PER_YARD)
    !byte   >(80*hole_c_PIXELS_PER_YARD)
    !byte   >(130*hole_c_PIXELS_PER_YARD)
;    !byte   >(170*hole_c_PIXELS_PER_YARD)
TREES_DIST_N = 10

TREES_HEIGHTS       !byte   14,13,12,11,9,8,6,5,4,3 ;,1
TREES_WIDTHS        !byte   7,5,5,5,3,3,3,3,3,2 ;,1
trees_l_TRUNK_HEIGHTS !byte   3,3,3,3,2,2,1,1,1,1 ;,0
TREES_TRUNK_WIDTHS  !byte   2,2,2,2,2,2,1,1,1,1 ;,0
TREES_TRUNK_ADJUST  !byte   1,1,1,1,1,1,2,2,2,0 ;,0

TREES_CMLO_ADDR_LO
    !for i,15 {
        !byte <(trees_collision_matrix_lo+(i-1)*40)
    } ; !for
TREES_CMLO_ADDR_HI
    !for i,15 {
        !byte >(trees_collision_matrix_lo+(i-1)*40)
    } ; !for
TREES_CMHI_ADDR_LO
    !for i,15 {
        !byte <(trees_collision_matrix_hi+(i-1)*40)
    } ; !for
TREES_CMHI_ADDR_HI
    !for i,15 {
        !byte >(trees_collision_matrix_hi+(i-1)*40)
    } ; !for
TREES_COLLISION_MATRIX_TOP_ROW = 4
TREES_COLLISION_MATRIX_BOTTOM_ROW = 18
; For use when checking collisions. 
; NOTE: actual depth is twice this value.
TREES_DEPTH = 5*hole_c_PIXELS_PER_FOOT

; Index is the tree terrain type.
TREES_TRUNK_COLOR_CODE_LOOKUP   !byte   3,1

; Patterns defined in order of decreasing size.
trees_l_PATTERN00    
    !byte   7
    !bin    "../../assets/trees/tree_pattern_00.bin"
trees_l_PATTERN01
    !byte   7
    !bin    "../../assets/trees/tree_pattern_01.bin"
trees_l_PATTERN02
    !byte   6
    !bin    "../../assets/trees/tree_pattern_02.bin"
trees_l_PATTERN03
    !byte   5
    !bin    "../../assets/trees/tree_pattern_03.bin"
trees_l_PATTERN04
    !byte   5
    !bin    "../../assets/trees/tree_pattern_04.bin"
trees_l_PATTERN05
    !byte   4
    !bin    "../../assets/trees/tree_pattern_05.bin"
trees_l_PATTERN06
    !byte   3
    !bin    "../../assets/trees/tree_pattern_06.bin"
trees_l_PATTERN07
    !byte   3
    !bin    "../../assets/trees/tree_pattern_07.bin"
trees_l_PATTERN08
    !byte   2
    !bin    "../../assets/trees/tree_pattern_08.bin"
trees_l_PATTERN09
    !byte   2
    !bin    "../../assets/trees/tree_pattern_09.bin"
;trees_l_PATTERN10
;    !byte   1
;    !bin    "../../assets/trees/tree_pattern_10.bin"

trees_l_PATTERNS_LO
    !byte   <trees_l_PATTERN00,<trees_l_PATTERN01,<trees_l_PATTERN02,<trees_l_PATTERN03,<trees_l_PATTERN04,<trees_l_PATTERN05,<trees_l_PATTERN06,<trees_l_PATTERN07,<trees_l_PATTERN08,<trees_l_PATTERN09   ;,<trees_l_PATTERN10
trees_l_PATTERNS_HI
    !byte   >trees_l_PATTERN00,>trees_l_PATTERN01,>trees_l_PATTERN02,>trees_l_PATTERN03,>trees_l_PATTERN04,>trees_l_PATTERN05,>trees_l_PATTERN06,>trees_l_PATTERN07,>trees_l_PATTERN08,>trees_l_PATTERN09   ;,>trees_l_PATTERN10
trees_l_WIDTHS  !byte   7,7,6,5,5,4,3,3,2,2 ;,1

trees_l_TILES_CLASS0
    !bin "../../assets/trees/tree_tiles_class00.bin"
    !bin "../../assets/trees/tree_tiles_class01.bin"
    !bin "../../assets/trees/tree_tiles_class02.bin"
    !bin "../../assets/trees/tree_tiles_class03.bin"
    !bin "../../assets/trees/tree_tiles_class04.bin"
    !bin "../../assets/trees/tree_tiles_class05.bin"
    !bin "../../assets/trees/tree_tiles_class06.bin"
    !bin "../../assets/trees/tree_tiles_class07.bin"
    !bin "../../assets/trees/tree_tiles_class08.bin"
    !bin "../../assets/trees/tree_tiles_class09.bin"
    !bin "../../assets/trees/tree_tiles_class10.bin"
    !bin "../../assets/trees/tree_tiles_class11.bin"
    !bin "../../assets/trees/tree_tiles_class12.bin"
    !bin "../../assets/trees/tree_tiles_class13.bin"
    !bin "../../assets/trees/tree_tiles_class14.bin"
    !bin "../../assets/trees/tree_tiles_class15.bin"
    !bin "../../assets/trees/tree_tiles_class16.bin"
    !bin "../../assets/trees/tree_tiles_class17.bin"
trees_l_MASKS_CLASS0
    !bin "../../assets/trees/tree_masks_class00.bin"
    !bin "../../assets/trees/tree_masks_class01.bin"
    !bin "../../assets/trees/tree_masks_class02.bin"
    !bin "../../assets/trees/tree_masks_class03.bin"
    !bin "../../assets/trees/tree_masks_class04.bin"
    !bin "../../assets/trees/tree_masks_class05.bin"
    !bin "../../assets/trees/tree_masks_class06.bin"
    !bin "../../assets/trees/tree_masks_class07.bin"
    !bin "../../assets/trees/tree_masks_class08.bin"
    !bin "../../assets/trees/tree_masks_class09.bin"
    !bin "../../assets/trees/tree_masks_class10.bin"
    !bin "../../assets/trees/tree_masks_class11.bin"
    !bin "../../assets/trees/tree_masks_class12.bin"
    !bin "../../assets/trees/tree_masks_class13.bin"
    !bin "../../assets/trees/tree_masks_class14.bin"
    !bin "../../assets/trees/tree_masks_class15.bin"
    !bin "../../assets/trees/tree_masks_class16.bin"
    !bin "../../assets/trees/tree_masks_class17.bin"
; NOTE: index into these tables is the tile 'class' ID.
trees_l_TILE_SELECTION_MASKS
    !byte   $07,$03,$03,$01,$03,$03,$01,$01,$01,$01,$01,$01,$01,$01,$03,$01,$01,$01
trees_l_TILE_BASE_OFFSETS       
    !byte   0,8,12,16,18,22,26,28,30,32,34,36,38,40,42,46,48,50

trees_l_TILES_ADDR_LO
    !for i,52 {
        !byte <trees_l_TILES_CLASS0+((i-1)*8)
    } ; !for
trees_l_TILES_ADDR_HI
    !for i,52 {
        !byte >trees_l_TILES_CLASS0+((i-1)*8)
    } ; !for
trees_l_MASKS_ADDR_LO
    !for i,52 {
        !byte <trees_l_MASKS_CLASS0+((i-1)*8)
    } ; !for
trees_l_MASKS_ADDR_HI
    !for i,52 {
        !byte >trees_l_MASKS_CLASS0+((i-1)*8)
    } ; !for

; Draw trunks at these offsets from char boundary.
trees_l_TRUNK_ADJUSTS   !byte   1,0,2,1,0,2,1,0,2,0 ;,0
trees_l_FOLIAGE_OFFSETS !byte   3,3,2,2,2,1,1,1,0,0 ;,0

trees_l_SHADOW_MASKS
    !byte   %11001100
    !byte   %00110011
    !byte   %11001100
    !byte   %00110011
    !byte   %11001100
    !byte   %00110011
    !byte   %11001100
    !byte   %11110011
    !byte   %11001100
    !byte   %00110011
    !byte   %11001100
    !byte   %00110011
    !byte   %11001100
    !byte   %00110011
    !byte   %11001100
    !byte   %00110011

trees_c_TRUNK_COLOR = RED


; *****************
; *** VARIABLES ***
; *****************
trees_n     !byte   0
trees_iter  !byte   0

trees_x_lo  !fill   trees_c_MAX_N
trees_x_hi  !fill   trees_c_MAX_N
trees_z_lo  !fill   trees_c_MAX_N
trees_z_hi  !fill   trees_c_MAX_N

trees_rot_x_lo  !fill   trees_c_MAX_N
trees_rot_x_hi  !fill   trees_c_MAX_N
trees_rot_z_lo  !fill   trees_c_MAX_N
trees_rot_z_hi  !fill   trees_c_MAX_N

; Some variables to hold intermediate results for the tree we're 
; currently drawing.
trees_proj_x        !byte   0
trees_proj_y        !byte   0
trees_depth_index   !byte   0
; Remainder after dividing by 8.
trees_extra_height  !byte   0
trees_row0          !byte   0
trees_col0          !byte   0
; NOTE: these are 'video-matrix' coordinates.
trees_trunk_row0    !byte   0
trees_trunk_col0    !byte   0

; Matrix from rows 4 to 18.
trees_collision_matrix_lo   !fill   15*40
trees_collision_matrix_hi   !fill   15*40
; While checking depth, store it here for current tree.  Then if we
; need to write to the collision matrix, this is the value we need.
trees_temp_z_lo !byte   0
trees_temp_z_hi !byte   0
trees_v_temp_in_front_of_target !byte   0

trees_drawing_order !fill   trees_c_MAX_N

; Used for determining which color code to use to draw tree trunks.
trees_v_color_code_totals   !fill   4


; *******************
; ****** MACROS *****
; *******************


; *******************
; *** SUBROUTINES ***
; *******************
; INPUTS:   P0-P1 = address of first byte of trees data.
!zone {
trees_load
    ldy #0
    lda (P0),y
    sta trees_n
    pha

    ; Make sure the pointer is updated before possibly exiting routine.
    lda P0
    clc
    adc #1
    sta P0
    lda P1
    adc #0
    sta P1

    pla
    beq .end

    ; X keeps track of tree number/index.
    ldx #0

.loop   
    lda (P0),y
    sta trees_x_lo,x
    iny
    lda (P0),y
    sta trees_x_hi,x
    iny
    lda (P0),y
    sta trees_z_lo,x
    iny
    lda (P0),y
    sta trees_z_hi,x

    ldy #0
    +utils_m_advance_zp_iter P0,4

    inx
    cpx trees_n
    bne .loop
    
    ; This only needs to be called once per hole.
    jsr trees_init_drawing_order

.end
    rts
; end sub trees_load
} ; !zone

; **************************************************

; NOTE: must call trees_set_aa_coords before calling this!
!zone {
.iter   !byte   0
.buffer !fill   4
.X_LO = 0
.X_HI = 1
.Z_LO = 2
.Z_HI = 3

trees_rotate
    ldx #0
    stx .iter

.loop
    ; Fill up buffer and use these variables for maths routines.
    lda trees_rot_x_lo,x
    sta .buffer+.X_LO
    lda trees_rot_x_hi,x
    sta .buffer+.X_HI
    lda trees_rot_z_lo,x
    sta .buffer+.Z_LO
    lda trees_rot_z_hi,x
    sta .buffer+.Z_HI

    lda #<(.buffer+.X_LO)
    sta MATHS0
    lda #>(.buffer+.X_LO)
    sta MATHS1
    lda #<(.buffer+.Z_LO)
    sta MATHS2
    lda #>(.buffer+.Z_LO)
    sta MATHS3
    clc
    jsr maths_s_rotate

    ; Write the (transformed) buffer values back into the array.
    ldx .iter
    lda .buffer+.X_LO
    sta trees_rot_x_lo,x
    lda .buffer+.X_HI
    sta trees_rot_x_hi,x
    lda .buffer+.Z_LO
    sta trees_rot_z_lo,x
    lda .buffer+.Z_HI
    sta trees_rot_z_hi,x

    inx
    stx .iter
    cpx trees_n
    bne .loop

    rts
; end sub trees_rotate
} ; !zone

; **************************************************

; NOTE: store axis-aligned coords in the 'rot' variables.  We don't need to
; store aa coords for collisions.
!zone {
trees_set_aa_coords
    ldx #0

.loop
    lda trees_x_lo,x
    sec
    sbc ball_world_x_lo
    sta trees_rot_x_lo,x
    lda trees_x_hi,x
    sbc ball_world_x_hi
    sta trees_rot_x_hi,x

    lda trees_z_lo,x
    sec
    sbc ball_world_z_lo
    sta trees_rot_z_lo,x
    lda trees_z_hi,x
    sbc ball_world_z_hi
    sta trees_rot_z_hi,x

    inx
    cpx trees_n
    bne .loop
    
    rts
; end sub trees_set_aa_coords
} ; !zone

; **************************************************

!zone {
; This is the index into trees_drawing_order.
.list_iter  !byte   0

trees_draw
    jsr trees_sort
    jsr trees_clear_collision_matrix
    jsr ttrunks_s_reset

    ldx #0
    stx .list_iter
    lda trees_drawing_order,x
    sta trees_iter
    tax

.loop
    ; We can quickly reject any trees which are either behind the camera or
    ; too far away.
    lda trees_rot_z_lo,x
    sta trees_temp_z_lo 
    sec
    sbc #trees_c_MAX_DISTANCE_LO
    lda trees_rot_z_hi,x
    sta trees_temp_z_hi 
    bmi .next
    sbc #trees_c_MAX_DISTANCE_HI
    +bge_s .next

    jsr trees_project
    bcs .next

    jsr trees_s_select_depth_index
    jsr trees_setup_draw
    jsr trees_s_draw_foliage2

    ldx trees_iter
    jsr trees_s_draw_trunk

.next
    inc .list_iter
    ldx .list_iter
    cpx trees_n
    beq .end
    lda trees_drawing_order,x
    sta trees_iter
    tax
    jmp .loop

.end
    rts
; end sub trees_draw
} ; !zone

; **************************************************

; INPUTS:   tree_iter = the tree we're dealing with
; OUTPUTS:  C clear if onscreen; C set if not visible
!zone {
trees_project
    ldx trees_iter

    lda #0
    sta P0
    sta P1
    lda trees_rot_z_lo,x
    sta P2
    lda trees_rot_z_hi,x
    sta P3
    jsr camera_project_onto_plate_y
    bcs .end
    lda P0
    sta trees_proj_y

    ldx trees_iter
    lda trees_rot_x_lo,x
    sta CAMERA0
    lda trees_rot_x_hi,x
    sta CAMERA1
    lda trees_rot_z_lo,x
    sta CAMERA2
    lda trees_rot_z_hi,x
    sta CAMERA3
    jsr camera_project_onto_plate_x

    ; Check if result is in bounds.
    ; Should be positive.
    lda CAMERA1
    bmi .out_of_bounds
    ; Divide by 2 (cf. m/c bitmap).
    lsr CAMERA1
    ror CAMERA0
    ; High byte should now be 0.
    lda CAMERA1
    bne .out_of_bounds
    ; And low byte < 160.
    lda CAMERA0
    cmp #160
    bcs .out_of_bounds
    sta trees_proj_x
    ; OK.
    clc
    rts ; EXIT POINT.

.out_of_bounds
    sec

.end
    rts
; end sub trees_project
} ; !zone

; **************************************************

!zone {
trees_s_select_depth_index
    ldx trees_iter
    ; Use Y as index into DIST table.
    ldy #0  

.loop
    lda trees_rot_z_lo,x
    sec
    sbc trees_l_DIST_LO,y
    lda trees_rot_z_hi,x
    sbc trees_l_DIST_HI,y
    +blt_s .found

    cpy #TREES_DIST_N-1
    beq .found
    iny
    jmp .loop

.found
    sty trees_depth_index
    rts
; end sub trees_s_select_depth_index
} ; !zone

; **************************************************

!zone {
trees_setup_draw
    lda trees_proj_y
    and #$07
    sta trees_extra_height

    ; Divide Y-position by 8 to find out what row this is.  Then subtract
    ; the height (measured in rows) to find the top row of the tree.
    lda trees_proj_y
    lsr
    lsr
    lsr
    ; We'll do something else with this value shortly.
    pha
    ldx trees_depth_index
    sec
    sbc TREES_HEIGHTS,x
    sta trees_row0
    ; Let's record where the trunk starts...
    pla
    sec
    sbc trees_l_TRUNK_HEIGHTS,x
    sta trees_trunk_row0

    ; NOTE: 4 pixels per byte!  So divide X-position by 4 to get the column.
    ; Then subtract half the width to get the first column.
    lda trees_proj_x
    lsr
    lsr
    sta trees_trunk_col0
    sec
    sbc trees_l_FOLIAGE_OFFSETS,x
    sta trees_col0

    rts
; end sub trees_setup_draw
} ; !zone

; **************************************************

!zone {
trees_sort
    lda #<trees_drawing_order
    sta P0
    lda #>trees_drawing_order
    sta P1
    lda trees_n
    sta P2
    ; Write address of 'compare' routine into bubble sort code.
    lda #<trees_compare
    sta utils_s_comp+1
    lda #>trees_compare
    sta utils_s_comp+2

    jsr utils_s_bubble_sort

    rts
; end sub trees_sort
} ; !zone

; **************************************************

; INPUTS:   X = tree index.
!zone {
.row_iter   !byte   0
.row_end    !byte   0
.char_row_count !byte   0
.trunk_width    !byte   0
.start_x    !byte   0
.end_x      !byte   0
.x_iter     !byte   0
.tree_index !byte   0

trees_s_draw_trunk
    stx .tree_index

    ldx trees_depth_index
    lda trees_l_TRUNK_HEIGHTS,x
    clc
    adc trees_trunk_row0
    sta .row_end
    lda trees_trunk_row0
    sta .row_iter
    lda TREES_TRUNK_WIDTHS,x
    sta .trunk_width
    ; Start and end columns (hi-res) for trunk.
    lda trees_proj_x
    and #$fc
    ora trees_l_TRUNK_ADJUSTS,x
    sta .start_x
    clc
    adc .trunk_width
    sta .end_x

    lda trees_depth_index
    cmp #6
    bcs .no_shadow
    lda .row_end
    sta P0
    lda trees_trunk_col0
    sta P1
    jsr trees_s_draw_shadow

.no_shadow
    ; Now draw the actual pixels.  The .row_iter and .row_end variables will
    ; now be used to hold bitmap-resolution rows.
    lda trees_trunk_row0
    asl
    asl
    asl
    sta .row_iter
    lda .row_end
    asl
    asl
    asl
    clc
    adc trees_extra_height
    sta .row_end

    ; TODO: record tree trunk depth and position now for this tree!
    ldx .tree_index
    ldy .start_x
-
    lda trees_rot_z_lo,x  
    sta ttrunks_v_depths_z_lo,y   
    lda trees_rot_z_hi,x  
    sta ttrunks_v_depths_z_hi,y   
    lda .row_iter
    sta ttrunks_v_y0,y            
    iny
    cpy .end_x
    bne -

    ; Loop starts here:
    lda .start_x
    sta .x_iter

-
    ldy .row_iter
    ldx .x_iter

    jsr trees_s_determine_color_code_for_trunk

    ; NOTE: if drawing into foreground, tree terrain must be respected - so
    ; color code is whatever's in MATHS6.  If drawing into the b/g, always use
    ; color code 3 to avoid clashes with the b/g line of trees.  (This is taken
    ; care of by call to trees_s_determine_color_code_for_trunk.)
    lda MATHS6
    jsr dp_s_draw_pixel

    inc .x_iter
    lda .x_iter
    cmp .end_x
    bne -

    inc .row_iter
    ldy .row_iter
    cpy .row_end
    beq .end
    ; Reset the pixel's x-coordinate.
    lda .start_x
    sta .x_iter
    jmp -
    
.end
    rts
; end sub trees_s_draw_trunk
} ; !zone

; **************************************************

!zone {
trees_clear_collision_matrix
    ldx #200
    lda #0
-
    sta trees_collision_matrix_lo-1,x
    sta trees_collision_matrix_lo+5*40-1,x
    sta trees_collision_matrix_lo+10*40-1,x
    sta trees_collision_matrix_hi-1,x
    sta trees_collision_matrix_hi+5*40-1,x
    sta trees_collision_matrix_hi+10*40-1,x
    dex
    bne -

    rts
; end sub trees_clear_collision_matrix
} ; !zone

; **************************************************

; INPUTS:   MATHS0 = row, MATHS1 = column
; NOTE: the depth of the current tree is stored in trees_temp_z_lo/hi. 
; NOTE: we must preserve the value of X.
!zone {
trees_write_collision_matrix
    ; First make sure that the row is in range.
    lda MATHS0
    cmp #TREES_COLLISION_MATRIX_TOP_ROW
    bcc .end
    cmp #TREES_COLLISION_MATRIX_BOTTOM_ROW+1
    bcs .end

    ; Put the address of low byte (of collision matrix) into P2-P3,
    ; and high byte into P4-P5.
    ; NOTE: this is the row address at 0th column.  Offset with value 
    ; stored in MATHS1 (= the column).
    ; NOTE: remember to subtract 4 from the row!!!
    lda MATHS0
    sec
    sbc #TREES_COLLISION_MATRIX_TOP_ROW
    tay
    lda TREES_CMLO_ADDR_LO,y
    sta P2
    lda TREES_CMLO_ADDR_HI,y
    sta P3
    lda TREES_CMHI_ADDR_LO,y
    sta P6
    lda TREES_CMHI_ADDR_HI,y
    sta P7

    ; Now let's see if the current depth (i.e. z-position) is less than the 
    ; one stored.  If yes, we'll overwrite it; as we will also do if the
    ; matrix contains 0 (meaning it's empty).
    ldy MATHS1
    lda (P2),y
    ora (P6),y
    beq .write

    lda trees_temp_z_lo
    sec
    sbc (P2),y
    lda trees_temp_z_hi
    sbc (P6),y
    +bge_s .end

.write
    ; BUG (fixed): sometimes tree will be at z=0, but if this is written into 
    ; the matrix it'll be interpreted as no foliage present.
    ; Quick hack-y fix - always add 1 to the z-position to avoid this error...
    lda trees_temp_z_lo
    clc
    adc #1
    sta (P2),y
    lda trees_temp_z_hi
    adc #0
    sta (P6),y

.end
    rts
; end sub trees_write_collision_matrix
} ; !zone

; **************************************************

; INPUTS:   P3 = index #1, P4 = index #2
; OUTPUTS:  C flag set if should be swapped, otherwise clear
!zone {
trees_compare
    ldx P3
    ldy P4

    ; Do the calculation A-B.  If A<B, items should be swapped.
    lda trees_rot_z_lo,x
    sec
    sbc trees_rot_z_lo,y
    lda trees_rot_z_hi,x
    sbc trees_rot_z_hi,y
    +blt_s .swap

    ; No swap.
    clc
    rts ; EXIT POINT.

.swap
    sec
    rts
; end sub trees_compare
} ; !zone

; **************************************************

!zone {
trees_init_drawing_order
    ldx #0
-   txa
    sta trees_drawing_order,x
    inx
    cpx #trees_c_MAX_N
    bne -
    rts
; end sub trees_init_drawing_order
} ; !zone

; **************************************************

!zone {
trees_s_draw_foliage2
    ; Record whether the current tree is behind or in front of the target.
    +clr trees_v_temp_in_front_of_target 
    lda trees_temp_z_lo
    sec
    sbc target_z_lo
    lda trees_temp_z_hi
    sbc target_z_hi
    +bge_s +
    inc trees_v_temp_in_front_of_target 
+

    ldx trees_depth_index
    ; Pattern matrix into P4-P5.
    lda trees_l_PATTERNS_LO,x
    sta P4
    lda trees_l_PATTERNS_HI,x
    sta P5
    lda trees_row0
    sta P0
    lda trees_col0
    sta P1

    lda #<trees_s_draw_tile
    sta ingm_mod0 
    lda #>trees_s_draw_tile
    sta ingm_mod0+1

    jsr ingm_s_draw_tile_pattern
    rts
; end sub trees_s_draw_foliage2
} ; !zone

; **************************************************

; NOTE: write this custom routine directly into ingm_s_draw_tile_pattern.
; INPUTS:   X = tile ID, BITMAP_LO/HI = destination,
;           FADE_CR_LO = column, FADE_CR_HI = row.
!zone {
trees_s_draw_tile
    txa
    bne +
    lda FADE_CR_HI
    cmp #13
    bcc +
    cmp #16
    bcs +
    ldx #17

+
    +utils_m_save_x_to_stack 

    jsr rand_s_get_fast
    and trees_l_TILE_SELECTION_MASKS,x
    clc
    adc trees_l_TILE_BASE_OFFSETS,x
    tax
    ; Use this (random) X as index into the address lookup tables.
    lda trees_l_TILES_ADDR_LO,x
    sta TREES_LO
    lda trees_l_TILES_ADDR_HI,x
    sta TREES_HI
    lda trees_l_MASKS_ADDR_LO,x
    sta VM_LO
    lda trees_l_MASKS_ADDR_HI,x
    sta VM_HI

    jsr trees_s_write_foliage_masks

    +utils_m_restore_x_from_stack 

.do_copy
    ; Copy 8 bytes from source->destination.
    ldy #0
    +utils_m_kernal_out
-
    lda (BITMAP_LO),y
    and (VM_LO),y
    ora (TREES_LO),y
    sta (BITMAP_LO),y
    iny
    cpy #8
    bne -
    +utils_m_kernal_in

    ; If this is the 'full' tile (ID=0), let's also write to the collision
    ; matrix (if appropriate).
    ; TODO: could also manipulate color table here!!!
    ; FIXME: group collision tiles together!
    txa
    beq +
    cmp #17
    bne .end
+
    ; Modulate color?
    ldy FADE_CR_HI
    cpy #16
    bcs .coll_matrix
    lda dp_l_VIDEO_RAM_ROWS_LO,y
    clc
    adc FADE_CR_LO
    sta COLORS_LO
    lda dp_l_VIDEO_RAM_ROWS_HI,y
    adc #0
    sta COLORS_HI
    ldy #0
    lda (COLORS_LO),y
    and #$0f
    ora #(YELLOW<<4)
    sta (COLORS_LO),y

.coll_matrix
    lda FADE_CR_HI 
    sta MATHS0
    lda FADE_CR_LO 
    sta MATHS1
    jsr trees_write_collision_matrix

.end
    rts
; end sub trees_s_draw_tile
} ; !zone

; **************************************************

; INPUTS:   P0 = bitmap row, P1 = char col + 1
!zone {
.MASK_BYTE = EDGES_LO

trees_s_draw_shadow
    ldx P1
    ; Don't bother if trunk is in column #0.
    beq .end
    dex
    txa
    asl
    asl
    tax
    lda P0
    asl
    asl
    asl
    tay

    ; Bitmap destination address into BITMAP_LO/HI.
    lda dp_l_BITMAP_ROWS_LO,y
    clc
    adc dp_l_BITMAP_COLS_LO,x
    sta BITMAP_LO
    lda dp_l_BITMAP_ROWS_HI,y
    adc dp_l_BITMAP_COLS_HI,x
    sta BITMAP_HI

    +utils_m_kernal_out 
    ldy #0
    ldx #0
-
    lda (BITMAP_LO),y
    and trees_l_SHADOW_MASKS,x
    sta (BITMAP_LO),y
    inx
    iny
    cpy #16
    bne -
    +utils_m_kernal_out 

.end
    rts
; end sub trees_s_draw_shadow
} ; !zone

; **************************************************

; INPUTS:   FADE_CR_LO = column, FADE_CR_HI = row,
;           VM_LO/HI = pointer to mask data
!zone {
trees_s_write_foliage_masks
    lda trees_v_temp_in_front_of_target 
    +branch_if_false .end
    lda FADE_CR_HI
    sec
    sbc target_v_overlap_char_b 
    clc
    adc #3
    bmi .end
    cmp #4
    bcs .end
    ; We have row offset - multiply by 16 to get index.
    asl
    asl
    asl
    asl
    sta CURSOR_POS_LO
    lda FADE_CR_LO
    sec
    sbc target_v_overlap_char_r
    clc
    adc #1
    bmi .end
    cmp #2
    bcs .end
    asl
    asl
    asl
    clc
    adc CURSOR_POS_LO
    tax
    ; Valid index now in X.
    ldy #0
-
    lda (VM_LO),y
    and target_v_foliage_masks,x
    sta target_v_foliage_masks,x
    inx
    iny
    cpy #8
    bne -

.end
    rts
; end sub trees_s_write_foliage_masks
} ; !zone

; **************************************************

; INPUTS:   Y = row (bitmap), trees_trunk_col0 (cf. 40*25 matrix)
; OUTPUTS:  MATHS6 = color code
!zone {
.BITMAP_ROW = PARTSYS_LO
.MATRIX_ROW = PARTSYS_HI

trees_s_determine_color_code_for_trunk
    ; Only if we're entering a new cell (where bitmap row is a multiple of
    ; 8) do we need to do anything...
    tya
    and #$07
    beq .work_to_do
    rts ; EXIT POINT.

.work_to_do
    +utils_m_save_xy_to_stack 

    ; Need matrix row (so divide bitmap row by 8).
    sty .BITMAP_ROW
    tya
    lsr
    lsr
    lsr
    sta .MATRIX_ROW
    cmp #16
    bcs +
    tay
    jsr trees_s_prepare_for_trunk_above_horizon
    jmp .clean_up
+

    ; Let's check if either color RAM or video RAM already set to trunk color.
    ; If they are, we'll go with that choice.
    ; Put color RAM address into P0-P1... 
    tay
    lda dp_l_COLOR_RAM_ROWS_LO,y
    clc
    adc trees_trunk_col0
    sta P0
    lda dp_l_COLOR_RAM_ROWS_HI,y
    adc #0
    sta P1
    ; ... and video RAM into P2-P3.
    lda dp_l_VIDEO_RAM_ROWS_LO,y
    clc
    adc trees_trunk_col0
    sta P2
    lda dp_l_VIDEO_RAM_ROWS_HI,y
    adc #0
    sta P3

    ldy #0
    lda (P0),y
    and #$0f
    cmp #trees_c_TRUNK_COLOR
    beq .use_code_3
    lda (P2),y
    and #$f0
    cmp #(trees_c_TRUNK_COLOR<<4)
    bne .analyze_bitmap

    ; So use code 1.
    lda #1
    +skip_2_bytes 
.use_code_3
    lda #3
    sta MATHS6
    jmp .clean_up

.analyze_bitmap
    ; Clear totals - we're only interested in slots 1 and 3.
    lda #0
    sta trees_v_color_code_totals+1   
    sta trees_v_color_code_totals+3   
    ; Base address of bitmap into PATTERN_LO-PATTERN_HI.
    ; Column must be multiplied by 4 for bitmap coordinates.
    lda trees_trunk_col0 
    asl
    asl
    tax
    ldy .BITMAP_ROW
    lda dp_l_BITMAP_ROWS_LO,y
    clc
    adc dp_l_BITMAP_COLS_LO,x
    sta PATTERN_LO
    lda dp_l_BITMAP_ROWS_HI,y
    adc dp_l_BITMAP_COLS_HI,x
    sta PATTERN_HI
    
    +utils_m_kernal_out
    ldy #7
-
    lda (PATTERN_LO),y
    jsr trees_s_count_color_codes
    dey
    bpl -
    +utils_m_kernal_in

    ; Set Y here - will use for indexing presently...
    ldy #0
    lda trees_v_color_code_totals+1
    cmp trees_v_color_code_totals+3
    beq .default
    bcs .in_front_of_sand
    ; So in front of water...
    lda #1
    sta MATHS6
    ; NOTE: video RAM address is in P2-P3.
    lda (P2),y
    and #$0f
    ora #trees_c_TRUNK_COLOR<<4
    sta (P2),y
    bne .clean_up
.default
.in_front_of_sand
    lda #3
    sta MATHS6
    ; NOTE: color RAM address is in P0-P1.
    lda #trees_c_TRUNK_COLOR
    sta (P0),y

.clean_up
    +utils_m_restore_xy_from_stack 
    rts
; end sub trees_s_determine_color_code_for_trunk
} ; !zone

; **************************************************

; INPUTS:   A = bitmap byte.
; NOTE: preserves value of Y.
!zone {
.mybyte !byte   0

trees_s_count_color_codes
    sta .mybyte
    +utils_m_save_y_to_stack 

    ldy #4
    lda .mybyte

    pha
.again
    ; Isolate leftmost pixel and use as index into 'totals' table.
    and #$03
    tax
    inc trees_v_color_code_totals,x
    pla
    dey
    beq .done
    pha
    lsr
    lsr
    jmp .again

.done
    +utils_m_restore_y_from_stack 
    rts
; end sub trees_s_count_color_codes
} ; !zone

; **************************************************

; Convenience routine - current cell needs to be prepared for trunk drawing.
; INPUTS:   Y = row (matrix)
!zone {
trees_s_prepare_for_trunk_above_horizon
    ; Put color RAM address into P0-P1.
    lda dp_l_COLOR_RAM_ROWS_LO,y
    clc
    adc trees_trunk_col0
    sta P0
    lda dp_l_COLOR_RAM_ROWS_HI,y
    adc #0
    sta P1

    ldy #0
    lda #trees_c_TRUNK_COLOR
    sta (P0),y
    ; And color code to be used goes in MATHS6.
    lda #3
    sta MATHS6

    rts
; end sub trees_s_prepare_for_trunk_above_horizon
} ; !zone

; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************

trees_c_SIZE = *-trees_c_BEGIN

