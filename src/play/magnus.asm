; Top-hole Golf
; Copyright 2020 Matthew Clarke


; NOTE: Inputs are two 3D vectors, representing ball velocity and spin axis.
; Spin axis is a unit vector, with each component stored as a fraction of
; 256.  Velocity vector has 16-bit components, an integer (high byte) and a
; fractional (low byte) part.
; Access to ball velocity is through ball_v_mutex_vx_lo, etc.  Spin axis never
; changes during flight so it's OK to use those variables directly.


fmag_c_BEGIN = *


; *****************
; *** CONSTANTS ***
; *****************
fmag_c_X_MASK = %0001
fmag_c_Y_MASK = %0010
fmag_c_Z_MASK = %0100


; *****************
; *** VARIABLES ***
; *****************


; *******************
; ****** MACROS *****
; *******************
!macro fmag_record_neg .addr, .mask {
    lda .addr
    eor #.mask
    sta .addr
} ; fmag_record_neg


; *******************
; *** SUBROUTINES ***
; *******************
; OUTPUT:   unit vector on which magnus force should be applied.  Stored in
;           ball_v_mutex_spin_x_lo, etc.
;           X = 'sign change'
!zone {
; Record here, for each component, whether sign needs changing (back) after
; normalization.  (See below for masks.)
.sign_change    !byte   0
.ITER = MATHS3
.SPIN_X_LO = LINE_X0_LO
.SPIN_X_HI = LINE_Y0_LO
.SPIN_Z_LO = LINE_X1_LO
.SPIN_Z_HI = LINE_Y1_LO

fmag_s_cross_product
    ; Look up spin axis and store in .SPIN_X_LO, etc.
    lda ball_v_spin_axis_x_lo
    sta .SPIN_X_LO
    lda ball_v_spin_axis_x_hi
    sta .SPIN_X_HI
    lda ball_v_spin_axis_z_lo
    sta .SPIN_Z_LO
    lda ball_v_spin_axis_z_hi
    sta .SPIN_Z_HI

    ; Clear this out first!
    lda #0
    sta .sign_change

    ; norm_x = vy*sz
    lda ball_v_mutex_vy_lo
    sta P0
    lda ball_v_mutex_vy_hi
    sta P1
    bpl +
    +neg16 P0
    +fmag_record_neg .sign_change,fmag_c_X_MASK
+
    lda .SPIN_Z_LO
    sta P2
    lda .SPIN_Z_HI
    sta P3
    bpl +
    +neg16 P2
    +fmag_record_neg .sign_change,fmag_c_X_MASK
+
    jsr maths_mul16

    ; That was an 8-bit number multiplied by a 16-bit number, so result should
    ; fit into 3 bytes.  Divide result by 256 - this will give us a 16-bit 
    ; value that should be interpreted as having integer (hi) and fractional
    ; (lo) parts.
    lda P5
    sta ball_v_mutex_spin_x_lo
    lda P6
    sta ball_v_mutex_spin_x_hi
    
    ; norm_y = vz*sx - vx*sz
    lda ball_v_mutex_vz_lo
    sta P0
    lda ball_v_mutex_vz_hi
    sta P1
    bpl +
    +neg16 P0
    +fmag_record_neg .sign_change,fmag_c_Y_MASK
+
    lda .SPIN_X_LO
    sta P2
    lda .SPIN_X_HI
    sta P3
    bpl +
    +neg16 P2
    +fmag_record_neg .sign_change,fmag_c_Y_MASK
+
    jsr maths_mul16
    ; Put result in MATHS0-MATHS2 temporarily.  (We don't need the
    ; 4th byte.)
    lda P4
    sta MATHS0
    lda P5
    sta MATHS1
    lda P6
    sta MATHS2
    ; If sign needs changing (because one of vz and sx was negative) change
    ; it now before subtraction.  NOTE: 24-bit value!
    lda .sign_change
    and #fmag_c_Y_MASK
    beq +   ; OK.
    +neg24 MATHS0
    ; Reset 'fmag_c_Y_MASK' bit to 0.
    +fmag_record_neg .sign_change,fmag_c_Y_MASK
+
    ; Now vx*sz.
    lda ball_v_mutex_vx_lo
    sta P0
    lda ball_v_mutex_vx_hi
    sta P1
    bpl +
    +neg16 P0
    +fmag_record_neg .sign_change,fmag_c_Y_MASK
+
    lda .SPIN_Z_LO
    sta P2
    lda .SPIN_Z_HI
    sta P3
    bpl +
    +neg16 P2
    +fmag_record_neg .sign_change,fmag_c_Y_MASK
+
    jsr maths_mul16
    ; Again, change sign of product if necessary before subtraction.
    ; Product is in P4-P6 (- we don't need the high byte).
    lda .sign_change
    and #fmag_c_Y_MASK
    beq +
    +neg24 P4
    ; Clear out 'fmag_c_Y_MASK' bit - may need it later...
    +fmag_record_neg .sign_change,fmag_c_Y_MASK
+
    ; Now a 24-bit subtraction.  Don't bother storing the low byte because
    ; we're multiplying by 256.
    lda MATHS0
    sec
    sbc P4
    lda MATHS1
    sbc P5
    sta ball_v_mutex_spin_y_lo
    lda MATHS2
    sbc P6
    sta ball_v_mutex_spin_y_hi
    ; Need to do a division later (to find unit vector) which will require
    ; positive values.  So if difference was negative, make it positive here
    ; and record change made.
    bpl +
    +neg16 ball_v_mutex_spin_y_lo   ;norm_y_lo
    +fmag_record_neg .sign_change,fmag_c_Y_MASK
+
    
    ; 0 - vysx.
    ; This could be either positive or negative.
    ; NOTE: SET AS NEGATIVE TO BEGIN WITH!!!
    +fmag_record_neg .sign_change,fmag_c_Z_MASK
    lda ball_v_mutex_vy_lo
    sta P0
    lda ball_v_mutex_vy_hi
    sta P1
    bpl +
    +neg16 P0
    +fmag_record_neg .sign_change,fmag_c_Z_MASK
+
    lda .SPIN_X_LO
    sta P2
    lda .SPIN_X_HI
    sta P3
    bpl +
    +neg16 P2
    +fmag_record_neg .sign_change,fmag_c_Z_MASK
+
    jsr maths_mul16
    lda P5
    sta ball_v_mutex_spin_z_lo
    lda P6
    sta ball_v_mutex_spin_z_hi

    ; So now we have a normal vector, stored in ball_v_mutex_spin_x_lo, etc.
    ; All values should (?!) be positive, but we've recorded (in .sign_change)
    ; any components that need to be negated before use.
    ; But still need to make it a unit vector.  First find its approximate
    ; length.
    lda ball_v_mutex_spin_x_hi
    sta P0
    lda ball_v_mutex_spin_y_hi
    sta P1
    lda ball_v_mutex_spin_z_hi
    sta P2
    jsr pythag_s_calc_magnitude

    ; |v^2| is in MATHS0-MATHS1; |v| is in X.
    stx MATHS2
    ; Divide each component of normal by MATHS2 (which holds approximate length 
    ; of normal).  Interpret the result as a fraction of 256.  If result is
    ; >255, clamp it to 255.  Store results back into norm_x_lo, etc., 
    ; always setting high byte to zero.

    ldx #0
.loop
    stx .ITER
    lda ball_v_mutex_spin_x_lo,x
    sta P0
    lda ball_v_mutex_spin_x_hi,x
    sta P1
    lda MATHS2
    sta P2
    lda #0
    sta P3
    jsr maths_div16
    ; FIXME: what if it's >16-bit?  Is this possible?!
    lda P1
    beq +
    ; Result is >255 so clamp to 255.
    lda #$ff
    bne ++
+
    lda P0
++
    ldx .ITER
    sta ball_v_mutex_spin_x_lo,x
    lda #0
    sta ball_v_mutex_spin_x_hi,x

    ; Increment twice because vectors are stored lo/hi, lo/hi, etc.
    inx
    inx
    cpx #6
    bne .loop

.done
    ; If the shot is a hook, each component of the spin vector must be negated.
    ; Obviously we will not change the actual vector here, but just the 
    ; '.sign_change' record.
    lda ball_v_spin_type
    cmp #ball_c_SPIN_TYPE_HOOK
    bne +
    lda .sign_change
    eor #%111
    sta .sign_change
+
    ldx .sign_change
    rts
; end sub fmag_s_cross_product
} ; !zone

; **************************************************

!zone {
fmag_calc_force
    rts
; end sub fmag_calc_force
} ; !zone

; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************

fmag_c_SIZE = *-fmag_c_BEGIN

