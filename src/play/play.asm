; Top-hole Golf
; Copyright 2020 Matthew Clarke


!to "play.o",cbm
!source "../core/labels.asm"
!source "../core/mymacros.asm"
!source "../core/sprite_data.asm"

; NOTE: this is to preserve the colors for the messaging area - otherwise
; the message disappears while game code loading!
*= gfxs_c_DISPLAY_BASE+24*40
    !fill   40,GREY3

*= $c400
!bin "../../assets/sprites/clubs.bin"
!bin "../../assets/sprites/balls.bin"
play_l_BLANK_SPRITE_FLAG    !fill   64,0
play_l_TURBINE_BLADE_SPRITES
    !bin "../../assets/sprites/turbine_blades.bin"
play_l_BIG_WHEEL_SPRITES
    !bin "../../assets/sprites/big_wheel.bin"
play_l_OVERHEAD_BALL_SPRITE
    !bin "../../assets/sprites/overhead_ball.bin"
play_l_MAX_STR_SPRITE
    !bin "../../assets/sprites/max_spr.bin"


*= $2000
    !byte <play_s_loop,>play_s_loop


play_c_BEGIN = *


; *****************
; *** CONSTANTS ***
; *****************
play_c_MODE_START_PLAY          = 0
play_c_MODE_PLAY                = 1
play_c_MODE_START_SCORE_CARDS   = 2
play_c_MODE_SCORE_CARDS         = 3
play_c_MODE_RESUME_PLAY         = 4
play_c_MODE_START_SCREEN_WIPE   = 5

play_c_SCORE_TABLE_CODE_STROKE_PLAY_FILENAME    !pet "sctblsp.prg",0
play_c_SCORE_TABLE_CODE_MATCH_PLAY_FILENAME     !pet "sctblmp.prg",0


; *****************
; *** VARIABLES ***
; *****************
play_v_current_mode !byte   play_c_MODE_START_PLAY


; *******************
; ****** MACROS *****
; *******************


; *******************
; *** SUBROUTINES ***
; *******************
!zone {
play_s_loop
    ; NOTE: multiload routines turn off the SuperCPU!
    +utils_m_turn_on_supercpu

    jsr snd_s_clear_regs

.loop_top
    lda play_v_current_mode
    cmp #play_c_MODE_START_PLAY
    beq .start_play
    cmp #play_c_MODE_START_SCORE_CARDS
    beq .start_score_cards
    cmp #play_c_MODE_SCORE_CARDS
    beq .score_cards
    cmp #play_c_MODE_START_SCREEN_WIPE
    beq .wipe
    jmp .loop_top

.start_play
    inc play_v_current_mode
    jsr round_init
    jsr interrupts_s_install
    jsr round_loop
    jmp .loop_top

.start_score_cards
    inc play_v_current_mode
    ; Turn off all sprites.
    +clr SPENA

    ; TODO: message to user that we're loading in some code?!
    ldx #msg_c_LOADING_SCORE_CARDS
    jsr msg_s_display_stock_msg 

    lda shared_v_is_match_play 
    +branch_if_true .match_play
    ldx #<play_c_SCORE_TABLE_CODE_STROKE_PLAY_FILENAME
    ldy #>play_c_SCORE_TABLE_CODE_STROKE_PLAY_FILENAME
    bne +
.match_play
    ldx #<play_c_SCORE_TABLE_CODE_MATCH_PLAY_FILENAME
    ldy #>play_c_SCORE_TABLE_CODE_MATCH_PLAY_FILENAME
+
    jsr CB_LOADFILE
    ; NOTE: the score_table module is loaded in here (@ quads_n) and the 
    ; subroutine we need to call (= sc_s_init) is at the very top of the file.
    jsr quads_n
    jsr msg_s_clear

    ; TODO: eventually, this may signal end of round and we'll want to 
    ; return from this subroutine.
    jmp .loop_top

.score_cards
    jsr play_s_check_score_cards_exit
    jmp .loop_top

.wipe
    lda #play_c_MODE_PLAY                
    sta play_v_current_mode
    lda #round_c_STATE_WIPING_SCREEN   
    sta round_current_state
    jsr transtn_s_init
    jsr round_loop
    jmp .loop_top

    rts
; end sub play_s_loop
} ; !zone

; **************************************************

!zone {
play_s_check_score_cards_exit
    ; Just listen out for a button press to resume play...
    ldx #joy_c_PORT2
    +joy_m_is_fire
    bne +
    +joy_m_is_locked_fire
    +branch_if_true ++

    lda #play_c_MODE_START_SCREEN_WIPE
    sta play_v_current_mode 
    +joy_m_lock_fire
    rts ; EXIT POINT.
+
    ; No button press so make sure lock is released.
    +joy_m_release_fire
++
    rts
; end sub play_s_check_score_cards_exit
} ; !zone

; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************
; **************************************************

play_c_SIZE = *-play_c_BEGIN

!source "backdrop.asm"
!source "slope.asm"
!source "ball.asm"
!source "camera.asm"
!source "clubs.asm"
!source "golfer.asm"
!source "hole.asm"
!source "ingame_gfx.asm"
!source "interrupts.asm"
!source "magnus.asm"
!source "maths.asm"
!source "particle_system.asm"
!source "players.asm"
!source "play_messages.asm"
!source "power_arc.asm"
!source "pythagoras.asm"
!source "quads.asm"
!source "target.asm"
!source "random.asm"
!source "round_manager.asm"
!source "score_cards.asm"
!source "../common/sound_engine.asm"
!source "sfx.asm"
!source "sprite_store.asm"
!source "stats.asm"
!source "tee_markers.asm"
!source "terrain_indicator.asm"
!source "trees.asm"
!source "tree_trunks.asm"
!source "waypoints.asm"
!source "wind.asm"
!source "wind_slope_shared.asm"
!source "transition.asm"
end_of_play

